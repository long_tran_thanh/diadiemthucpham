<br/>
<form method="POST" action="/admin/configuration/update">
<div class="row">
	<div class="col-md-6" >
		<div class="form-group">
			<label>Tiêu đề website trên trình duyệt </label>
			<input type="text" value="<?= @$jsonConfigData['title'] ?>" name="title" class="form-control">
		</div>

		<div class="form-group">
			<label>Tên công ty </label>
			<input type="text" value="<?= @$jsonConfigData['company'] ?>" name="company" class="form-control">
		</div>

		<div class="form-group">
				<label>Email </label>
				<input type="text" value="<?= @$jsonConfigData['email'] ?>" name="email" class="form-control">
			</div>

		<div class="form-group">
			<label>Logo </label>
			<input name="logo" class="form-control" value="<?= @$jsonConfigData['logo'] ?>" />
		</div>

		<div class="form-group">
			<label>Logo dưới footer</label>
			<input name="logo_footer" class="form-control" value="<?= @$jsonConfigData['logo_footer'] ?>" />
		</div>

		


		<button type="submit" class="btn btn-primary ">Lưu dữ liệu</button>
	</div>

	<div class="col-md-6" rel="edit">

		<div class="form-group">
			<div class="form-group">
				<label>Địa chỉ </label>
				<textarea name="company_address" class="form-control" ><?= @$jsonConfigData['company_address'] ?></textarea>
			</div>


			
			<label>Meta Tag Description</label>
			<textarea name="meta_description" class="form-control" ><?= @$jsonConfigData['meta_description'] ?></textarea>
		</div>

		<div class="form-group">
			<label>Meta Tag Keywords</label>
			<textarea name="meta_tag_keyword"class="form-control"  ><?= @$jsonConfigData['meta_tag_keyword'] ?> </textarea>
		</div>

		<div class="form-group">
			<label>Meta author</label>
			<textarea name="meta_author" class="form-control"  ><?= @$jsonConfigData['meta_author'] ?></textarea>
		</div>

		<div class="form-group">
			<label>Bản quyền footer </label>
			<input type="text" value="<?= @$jsonConfigData['footer_content'] ?>" name="footer_content" class="form-control">
		</div>

	</div>
</div>
</form>



