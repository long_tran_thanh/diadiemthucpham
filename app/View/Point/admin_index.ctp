<div class="mt-5"></div>

<table class='table table-bordered  table-hover' >
	<tr>
		<th><?php echo $this->Paginator->sort('id', 'id');?></th>
		<th><?php echo $this->Paginator->sort('org_type', 'Loại hình');?></th>

		<th><?php echo $this->Paginator->sort('name', 'Tên tổ chức, cá nhân');?></th>
		<th><?php echo $this->Paginator->sort('formatted_address', 'Địa chỉ');?></th>
		<th><?php echo $this->Paginator->sort('phone', 'Số điện thoại');?></th>

		<th><?php echo $this->Paginator->sort('total_product', 'Tổng sản phẩm');?></th>
        <th><?php echo $this->Paginator->sort('total_enroll', 'Tổng người nhận');?></th>
		<th><?php echo $this->Paginator->sort('is_sale', 'Bán');?></th>
        <th><?php echo $this->Paginator->sort('is_free', 'Cho');?></th>
        <th><?php echo $this->Paginator->sort('actived', 'Tình trạng');?></th>
        <th><?php echo $this->Paginator->sort('created_at', 'Thời gian tạo');?></th>
        <th>#</th>
	</tr>

    <?php foreach ($pointData as $key  => $data){ ?>
	<tr>
		<td><?php echo $data['Point']['id']; ?></td>
		<td><?php echo $data['Point']['org_type']; ?></td>
		<td><?php echo $data['Point']['name']; ?></td>
		<td><?php echo $data['Point']['formatted_address']; ?></td>
		<td><?php echo $data['Point']['phone'];?></td>
        <td class="text-right"><?php echo $data['Point']['total_product'];?></td>
        <td class="text-right"><?php echo $data['Point']['total_enroll'];?></td>

		<td>
            <?php if( $data['Point']['is_sale'] == true ){?>
                <span class="badge badge-primary"> Bán </span>
            <?php } ?> 
        </td>

        <td>
            <?php if( $data['Point']['is_free'] == true ){?>
                <span class="badge badge-info"> Cho </span>
            <?php } ?> 
        </td>

        <td>
            <a href="/admin/point/toogle?order_id=<?php echo $data['Point']['id']; ?>">
            <?php if( $data['Point']['actived'] == false ){?>
                <span class="badge badge-danger"> Chưa kích hoạt </span>
            <?php }else{ ?> 
                <span class="badge badge-success"> Kích hoạt </span>
            <?php } ?>
            </a>

        </td>

		<td><?php echo date('d-m-Y H:i', strtotime($data['Point']['created_at'])); ?></td>
        <td>
            <a class="small" href="javascript:void(0)" ref="order-list" order-id="<?php echo $data['Point']['id']; ?>"> [Chi tiết] </a> | 
            <a class="small" href="/admin/point/enroll?order_id=<?php echo $data['Point']['id']; ?>"> [DS Nhận] </a>
        </td>
	</tr>
    <?php } ?>
</table>

<?php if((int)$this->Paginator->counter('{:pages}')>1){ ?>
<div class='pagination pull-right'>
	<ul class='pagination pagination-sm'>
	<?php
		echo $this->Paginator->first('<<',array('tag' => 'li'));
		echo $this->Paginator->prev('<', array('tag' => 'li'), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a'));
		echo $this->Paginator->numbers(array('separator' => '','currentTag' => 'a', 'currentClass' => 'active','tag' => 'li','first' => 1));
		echo $this->Paginator->next('>', array('tag' => 'li','currentClass' => 'disabled'), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a'));
		echo $this->Paginator->last('>>',array('tag' => 'li'));
	?>
	</ul>
	<p><small class='pull-right'>
		<?php echo $this->Paginator->counter('Trang số {:page} trong {:pages} trang, hiển thị {:current} dòng trong tổng số {:count} dòng '); ?></small>
	</p>
</div>
<?php }?>


<div class="modal fade" id="modal-order-item" tabindex="-1" role="dialog"  aria-hidden="true" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title font-weight-bold text-danger" >[+ Danh sách đơn hàng ]</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            
            <div class="modal-body">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Thoát</button>
            </div>
        
        </div>
    </div>
</div>



<script type="text/javascript">
    $(document).ready(function(){

        $('a[ref=order-list]').click(function(){
            
            $.get('/admin/point/detail?order_id='+$(this).attr('order-id'), function(resp){
                $('#modal-order-item').find('div[class=modal-body]').html(resp)
                $('#modal-order-item').modal();
            });
        })
    })
</script>