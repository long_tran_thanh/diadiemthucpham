
<table class="table table-bordered " id="file-list">
    <thead>
        <tr class="table-primary">
            <th >STT</th>
            <th >Hình thức </th>
            <th >Loại </th>
            <th >Tên </th>
            <th >Số lượng </th>
            <th >Giá bán </th>
            <th >Thời gian </th>
            <th >Ghi chú </th>
        </tr>
    </thead>
    <tbody>
        <?php $inc=1;?>
        <?php foreach($pointItems as $id=> $pointItem){ ?> 
            <tr>
                <td><?php echo $inc++;?></td>
                <th><?php echo $pointItem['PointItem']['method'];?></th>
                <td><?php echo $pointItem['PointItem']['product_type'];?></td>
                <td><?php echo $pointItem['PointItem']['product_name'];?></td>

                <td class="text-right text-danger" >
                    <?php echo @number_format($pointItem['PointItem']['amount']);?>
                </td>
                <td class="text-right text-danger">
                    <?php echo @number_format($pointItem['PointItem']['price']);?>
                </td>

                <?php if($pointItem['PointItem']['start_date']!="" && $pointItem['PointItem']['end_date']!=""){ ?>          
                    <td>
                        <?php if($pointItem['PointItem']['start_date'] == $pointItem['PointItem']['end_date'] ){?>
                        
                            Từ <?php echo date('d-m-Y', strtotime($pointItem['PointItem']['start_date']) );?> đến <?php echo date('d-m-Y', strtotime($pointItem['PointItem']['end_date']) );?>
                        <?php } else { ?>
                            Trong ngày <?php echo date('d-m-Y', strtotime($pointItem['PointItem']['start_date']) );?>
                        <?php } ?>
                    </td>
                <?php } else {?>
                    <td>
                        <?php echo $pointItem['PointItem']['schedule'];?>
                    </td>
                <?php } ?>
                
                <td> <?php echo $pointItem['PointItem']['desc'];?></td>
            </tr>
        <?php } ?>
    </tbody>
</table>