<div class="mt-5"></div>

<table class='table table-bordered  table-hover' >
	<tr>
		<th><?php echo $this->Paginator->sort('id', 'id');?></th>
		<th><?php echo $this->Paginator->sort('name', 'Họ tên');?></th>
		<th><?php echo $this->Paginator->sort('phone', 'Số điện thoại');?></th>		
		<th><?php echo $this->Paginator->sort('phone', 'Thời gian gởi yêu cầu');?></th>
	</tr>

    <?php foreach ($customers as $key  => $customer){ ?>
	<tr>
		<td><?php echo $customer['Customer']['id']; ?></td>
		<td><?php echo $customer['Customer']['name']; ?></td>
		<td><?php echo $customer['Customer']['phone']; ?></td>		
		<td><?php echo date('d-m-Y H:i', strtotime($customer['Customer']['created_at'])); ?></td>        
	</tr>
    <?php } ?>
</table>
