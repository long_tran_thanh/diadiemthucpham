<div id="container">
	<div id="sidebar">
		<div class="sidebar-wrapper">
			<div class="panel panel-default" id="features">
				<div class="panel-heading">
					<h3 class="panel-title ">Có <span class="text-danger"><?php echo count($points); ?> </span> địa điểm
					<button type="button" class="btn btn-xs btn-default pull-right" id="sidebar-hide-btn"><i class="fa fa-chevron-left"></i></button></h3>
				</div>
				<div class="panel-body">
					<div class="row">
						<div class="col-xs-8 col-md-8">
							<input type="text" class="form-control search" placeholder="Tìm kiếm" />
						</div>
						<div class="col-xs-4 col-md-4">
							<button type="button" class="btn btn-primary pull-right sort" data-sort="feature-name" id="sort-btn"><i class="fa fa-sort"></i>&nbsp;&nbsp;Sort</button>
						</div>

					</div>
				</div>
				<div class="sidebar-table">
					<table class="table table-hover mt-5" id="feature-list">
						<thead class="hidden">
							<tr>
								<th>Danh sách</th>
							<tr>
						</thead>
						<tbody class="list">
							<?php foreach($points as $pointId => $point) { ?>
								<tr>
									<td ref="infor" order-id="<?php echo $point['Point']['id'];?>">
										<span class="text-danger"><strong><?php echo $point['Point']['name'];?></strong></span>
											<ul class="list-unstyled point-address">
												<li><span class="glyphicon glyphicon-map-marker" aria-hidden="true"></span>&nbsp;&nbsp;<?php echo $point['Point']['formatted_address'];?></li>
												<li>Tổ chức : <strong><?php echo $point['Point']['org_type'];?></strong></li>
												<li>Hình thức: <strong>
													<?php if($point['Point']['is_free'] == True){ ?>
													<span class="label label-primary">Bán</span>&nbsp;
													<?php }?> 

													<?php if($point['Point']['is_sale'] == True){ ?>
														<span class="label label-success">Cho </span>&nbsp;
                                                    <?php }?> 
													</strong>
												</li>
											</ul>
										<p>
									</td>
								</tr>
							<?php } ?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
	<div id="map"></div>
</div>


<div class="modal fade" id="enroll-modal" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Đăng ký !</h4>
      </div>
      <div class="modal-body">
        <form method="POST" action="/" id="enroll-form">
			<input type="hidden" name="token_key" value="<?php echo $token_key ?>"/>
			<input type="hidden" name="order_id" value=""/>
			<div class="form-group">
				<label>Họ tên <span class="text-danger">(*)</span></label>
				<input type="text" 
						maxlength="50" name="fullname" 
						data-parsley-error-message="Thiếu thông tin họ tên người cần nhận (mua) hàng"														
						required="required" 
						class="form-control" >
			</div>
			<div class="form-group">
				<label >Số điện thoại <span class="text-danger">(*)</span></label>
				<input type="text" 
						name="phone"  
						maxlength="22" 
						required="required" class="form-control" 
						data-parsley-error-message="Thiếu thông tin số điện thoại"
				>
			</div>
			<p><small><span class="text-danger">(*)</span> Bắt buộc phải nhập </small></p>
		</form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
        <button type="button" class="btn btn-success" id="btn-pick-order">Đăng ký</button>
      </div>
    </div>
  </div>
</div>
