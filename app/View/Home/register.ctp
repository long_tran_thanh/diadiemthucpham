
<style>
html, body, #container {
    overflow: auto !important;
}
</style>
<div class="container mt-5">
    <div class="col-md-12 ">
        <div class="panel panel-success mt-5 ">
            <div class="panel-heading">Thông tin đơn vị, cá nhân cung cấp thực phẩm </div>
            <div class="panel-body">
                <form method="POST" action="/dang-ky-phan-phoi-thuc-pham" id="order_form"> 
                <input type="hidden" minlength=0
                    name="orders" 
                    value="" 
                    required data-parsley-error-message="Thiếu thông về đơn hàng "
                    data-parsley-errors-container="#error_order_list">

                <input type="hidden" name="province_name" value=""/>
                <input type="hidden" name="district_name" value=""/>
                <input type="hidden" name="ward_name" value=""/>
                <input type="hidden" name="token_key" value="<?php echo $token_key ?>"/>
                <div class="form-group  mt-2">
                    <label> Loại hình: &nbsp;&nbsp;</label>
                    <label class="radio-inline">
                        <input type="radio" name="org_type" value="Cá nhân" checked="checked"> Cá nhân&nbsp;&nbsp;
                    </label>
                    <label class="radio-inline">
                        <input type="radio" name="org_type" value="Tổ chức"> Tổ chức&nbsp;&nbsp;
                    </label>

                    <label class="radio-inline">
                        <input type="radio" name="org_type" value="Hộ gia đình"> Hộ gia đình&nbsp;&nbsp;
                    </label>

                    <label class="radio-inline">
                        <input type="radio" name="org_type" value="Xe lưu động"> Xe lưu động&nbsp;&nbsp;
                    </label>
                </div>

                <div class="form-group">
                    <label for="exampleInputEmail1">Đơn vị cung cấp <span class="text-danger">(*)</span></label>
                    <input type="text" class="form-control" name="org_name" placeholder="Họ tên người cung cấp, tên đơn vị" 
                        value="Công ty TNHH ABC "
                        required="required" 
                        data-parsley-error-message="Thiếu thông tin họ tên hoặc đơn vị cung cấp ">
                </div>

                <div class="form-group">
                    <label for="exampleInputEmail1">Số điện thoại liên hệ <span class="text-danger">(*)</span></label>
                    <input type="text" class="form-control" name="org_phone"  placeholder="Số điện thoại liên hệ"
                        required="required"
                        value="092578248"
                        data-parsley-error-message="Thiếu thông tin số điện thoại liên hệ ">
                </div>

                <div class="form-group">
                    <label for="exampleInputPassword1">Địa chỉ liên hệ <span class="text-danger">(*)</span></label>
                    <input type="text" class="form-control" name="org_address"  placeholder="201/12 Trần Hưng Đạo"
                        value="341 Sư Vạn Hạnh, Phường 10, Quận 10, Thành phố Hồ Chí Minh"
                        required="required"
                        data-parsley-error-message="Thiếu thông tin địa chỉ liên hệ">
                </div>
                
                <div class="row ">
                    <div class="form-group col-xs-12  col-sm-5 col-md-4  ">
                        <label> Tỉnh/Thành phố <span class="text-danger">(*)</span></label>
                        <select ref="province_code" name="org_province"  class="form-control">                                
                            <?php foreach($provinces as $province){?>                                
                                <option value="<?php echo $province['Cadastral']['province_code'];?>" 
                                    <?php echo ( $province['Cadastral']['province_code']=="79"?'selected="selected"':'');?>>
                                    <?php echo $province['Cadastral']['province_name'];?>
                                </option>
                            <?php }?>
                        </select>
                    </div>
                    
                    <div class="form-group col-xs-12  col-sm-5 col-md-4  ">
                        <label> Quận/Huyện <span class="text-danger">(*)</span> </label>
                        <select ref="district_code"  name="org_district"  class="form-control">
                        <option value="" selected="selected"> --- </option>
                        <?php foreach($districts as $district){?>                                
                            <option value="<?php echo $district['Cadastral']['district_code'];?>">
                                <?php echo $district['Cadastral']['district_name'];?>
                            </option>
                        <?php }?>
                        </select>
                    </div>

                    <div class="form-group ol-xs-12  col-sm-5 col-md-4  ">
                        <label> Phường/Xã </label>
                        <select ref="ward_code" name="org_ward" class="form-control">
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <label for="">Hình thức: &nbsp;&nbsp;</label>
                    <label class="radio-inline">
                    <input type="radio" name="method" label="Cho" value="FREE" checked="checked"> Cho </label>
                    <label class="radio-inline">
                    <input type="radio" name="method" label="Bán" value="SALE"> Bán  </label>
                </div>


                <div class="form-group" ref="FOOD_TYPE" style="display:none">
                    <label for="">Lựa chọn: &nbsp;&nbsp;</label>
                    <label class="radio-inline">
                    <input type="radio" name="food_type" value="SALE" checked="checked"> Thực phẩm  </label>
                    <label class="radio-inline">
                    <input type="radio" name="food_type" value="SALE_COURSE" value="SALE_COURSE"> Xuất ăn </label>
                </div>

                <hr/>

                <div class="mt-5" >
                    <div class="form-group" ref="method" vd="FREE" pt="parent">
                        <div class="form-group">
                            <label>Loại thực phẩm, đồ ăn <span class="text-danger">(*)</span></label>
                            <input type="text" ref="order-items" placeholder="Hộp cơm phân, rau củ quả, gạo, bắp....." 
                                    class="form-control" 
                                    name="product_name"
                                    value="Cơm hộp "          
                                    maxlength="255" >
                        </div>
                        <div class="form-group">
                            <label>Số lượng <span class="text-danger">(*)</span></label>
                            <input type="text" ref="order-items" class="form-control" name="amount" value="200" ">
                        </div> 
                        
                        <div class="row ">
                            <div class="form-group col-xs-12  col-sm-5 col-md-4  ">
                                <label>Thời gian </label>
                                <select name="sell_food_schedule"  class="form-control">
                                    <option value="date_range" selected="selected" > Trong thời gian </option>
                                    <option value="date_schedule" > Định kỳ theo thời gian </option>
                                </select>
                            </div>

                            <div
                                class="form-group col-xs-12  col-sm-5 col-md-4" 
                                ref="sell_food_schedule" 
                                vd="date_range">
                                <label>Từ ngày </label>
                                <input type="text" class="form-control" ref="order-items"  dp="datepicker" name="from_date" value="<?php echo date('d-m-Y');?>" >
                            </div>
                            
                            <div class="form-group ol-xs-12  col-sm-5 col-md-4" 
                                ref="sell_food_schedule" 
                                vd="date_range">
                                <label>Đến ngày</label>
                                <input type="text" class="form-control" ref="order-items"  dp="datepicker"  name="to_date" value="<?php echo date('d-m-Y');?>" >
                            </div>

                            <div class="form-group col-xs-12  col-sm-5 col-md-8" ref="sell_food_schedule" vd="date_schedule"  style="display:none" >
                                <label>Thời gian định kỳ</label>
                                <input type="text" class="form-control" name="schedule" ref="order-items" value="" placeholder="Thứ ba và thứ năm hàng tuần">
                            </div>                                
                        </div>

                        <div class="form-group">
                            <label>Ghi chú </label>
                            <textarea ref="order-items" class="form-control" name="desc">Ưu tiên người già yếu, gia đình khó khăn</textarea>
                        </div>
                        <button type="button" class="btn btn-success pull-right" ref="add_item_order"> Ghi vào danh sách </button>
                    </div>

                    <div class="form-group" ref="method" vd="SALE" style="display:none" pt="parent">
                        <input type="hidden" ref=order-items value="Thực phẩm" name="product_type">

                        <div class="form-group">
                            <label>Loại thực phẩm, xuất ăn <span class="text-danger">(*)</span></label>
                            <input type="text"
                                    ref=order-items
                                    placeholder="Rau cải, cà chua, gao, thịt heo..." 
                                    class="form-control" 
                                    name="product_name"
                                    value="Gạo ăn " 
                                    maxlength="255" >
                            <small class="text-muted">Ghi từng lần mỗi loại cụ thể </small>
                        </div> 
                        <div class="row ">
                            <div class="form-group col-xs-12  col-sm-5 col-md-6">
                                <label>Số lượng <span class="text-danger">(*)</span></label>
                                <input type="text"
                                    ref=order-items
                                    class="form-control" name="amount" value="200">
                            </div>

                            <div class="form-group col-xs-12  col-sm-5 col-md-6">
                                <label>Giá bán (theo kg, gói...)</label>
                                <input type="number" ref=order-items class="form-control" name="price" value="25000" >
                            </div>
                        </div>

                        <div class="row ">
                            <div class="form-group col-xs-12  col-sm-5 col-md-4  ">
                                <label>Thời gian </label>
                                <select name="sell_food_schedule"  class="form-control">
                                    <option value="date_range" selected="selected" > Trong thời gian </option>
                                    <option value="date_schedule" > Định kỳ theo thời gian </option>
                                </select>
                            </div>

                            <div
                                class="form-group col-xs-12  col-sm-5 col-md-4" 
                                ref="sell_food_schedule" 
                                vd="date_range">
                                <label>Từ ngày </label>
                                <input type="text" class="form-control" ref="order-items"  dp="datepicker" name="from_date" value="<?php echo date('d-m-Y');?>" >
                            </div>
                            
                            <div class="form-group ol-xs-12  col-sm-5 col-md-4" 
                                ref="sell_food_schedule" 
                                vd="date_range">
                                <label>Đến ngày</label>
                                <input type="text" class="form-control" ref="order-items"  dp="datepicker"  name="to_date" value="<?php echo date('d-m-Y',strtotime('+ 1 month'));?>" >
                            </div>

                            <div class="form-group col-xs-12  col-sm-5 col-md-8" ref="sell_food_schedule" vd="date_schedule"  style="display:none" >
                                <label>Thời gian định kỳ</label>
                                <input type="text" class="form-control" name="schedule" ref="order-items" value="" placeholder="Thứ ba và thứ năm hàng tuần">
                            </div>                                
                        </div>

                        <div class="form-group">
                            <label>Ghi chú </label>
                            <textarea
                                class="form-control"
                                ref=order-items
                                name="desc"> Giảm giá cho gia đình khó khăn </textarea>
                        </div>
                        <button type="button" class="btn btn-success pull-right" ref="add_item_order"> Ghi vào danh sách </button>
                    </div>

                    <div class="form-group" ref="method" vd="SALE_COURSE" style="display:none">
                        <input type="hidden" ref=order-items value="Suất ăn" name="product_type">
                        <div class="form-group">
                            <label>Phần ăn, suất ăn <span class="text-danger">(*)</span></label>
                            <input type="text" 
                                placeholder="Cơm hộp, cơm phần, mì, hủ tíu, phở......" 
                                class="form-control" 
                                name="product_name" 
                                value="Cơm phần" 
                                ref="order-items"
                                maxlength="255" >
                        </div>  

                        <div class="row ">
                            <div class="form-group col-xs-12  col-sm-5 col-md-6">
                                <label>Số lượng (hộp, phần, bich....) <span class="text-danger">(*)</span> </label>
                                <input type="number" ref="order-items" class="form-control" name="amount" value="" >
                            </div>

                            <div class="form-group col-xs-12  col-sm-5 col-md-6">
                                <label>Giá bán</label>
                                <input type="number" ref="order-items" class="form-control" name="price" value="0">
                            </div>
                        </div>

                        <div class="row ">
                            <div class="form-group col-xs-12  col-sm-5 col-md-4  ">
                                <label>Thời gian </label>
                                <select name="sell_food_schedule" class="form-control">
                                    <option value="date_range" selected="selected" > Trong thời gian </option>
                                    <option value="date_schedule" > Định kỳ theo thời gian </option>
                                </select>
                            </div>

                            <div
                                class="form-group col-xs-12  col-sm-5 col-md-4" 
                                ref="sell_food_schedule" 
                                vd="date_range">
                                <label>Từ ngày </label>
                                <input type="text" class="form-control " ref="order-items"  dp="datepicker" name="from_date" value="">
                            </div>
                            
                            <div class="form-group ol-xs-12  col-sm-5 col-md-4" 
                                ref="sell_food_schedule" 
                                vd="date_range">
                                <label>Đến ngày</label>
                                <input type="text" class="form-control " ref="order-items"  dp="datepicker" name="to_date" value="" >
                            </div>

                            <div class="form-group col-xs-12  col-sm-5 col-md-8" ref="sell_food_schedule" vd="date_schedule"  style="display:none" >
                                <label>Thời gian định kỳ</label>
                                <input type="text" class="form-control" name="schedule" ref="order-items" value="" placeholder="Thứ ba và thứ năm hàng tuần">
                            </div>
                        </div>

                        <div class="form-group">
                            <label>Ghi chú </label>
                            <textarea class="form-control"  ref="order-items" name="desc"></textarea>
                        </div>

                        <button type="button" class="btn btn-success pull-right" ref="add_item_order"> Ghi vào danh sách </button>
                    </div>

                    <div class="clearfix"></div>
                    <p class="text-uppercase mt-5"><strong> Danh sách đơn hàng </strong></p>
                    
                    <p class="text-danger" id="error_order_list"></p>
                    <table class="table table-bordered">
                        <tr class="info">
                            <th  >STT</th>
                            <th  >Hình thức </th>
                            <th  >Loại </th>
                            <th >Tên </th>
                            <th  >Số lượng </th>
                            <th  >Giá bán </th>
                            <th >Thời gian </th>
                            <th  >Ghi chú </th>
                            <th ></th>
                        </tr>
                        <tbody id="tbody_order_list">
                            <tr>
                            <td colspan="10" class="text-center">
                                Không có dữ liệu 
                            </td>
                            </tr>
                        </tbody>

                    </table>
                </div>

                <hr/>
                <div class="form-group" ref="password_sale" style=display:none>
                    <label>Mật khẩu đăng nhập </label>
                    <input type="text" class="form-control" name="password_sale" disabled=disabled value="" maxlength="20" required="required"
                    data-parsley-error-message="Nhập mật khẩu để đăng nhập hệ thống">
                    <small class="text-muted">Mật khẩu đăng nhập để quản lý sản phẩm bán! </small>
                </div>

                <div class="form-group form-check mt-2">
                    <input type="checkbox" value="1" class="form-check-input" 
                    name="accept_condition"
                    required="required"
                    data-parsley-checkmin="1" 
                    data-parsley-error-message="Xác nhận việc đánh dấu thông tin!...">
                    <label class="form-check-label"  >Tôi xác nhận thông tin</label>
                </div>
                <p>
                <span class="text-danger">(*)</span> : Bắt buộc phải nhập thông tin </p>

                <button type="submit" class="btn btn-primary">Gửi thông tin </button>
            </form>            
            </div>
        </div>
    </div>
</div>    