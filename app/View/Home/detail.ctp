<p class="text-uppercase "><strong>Thông tin:&nbsp;<span class="text-danger">
    <?php echo $point['Point']['name'];?></span></strong>
</p>
<ul class="list-unstyled">
    <li><strong>Tổ chức</strong>: <?php echo $point['Point']['org_type'];?></li>
    <li><strong>Địa chỉ</strong>: <?php echo $point['Point']['formatted_address'];?></li>
</ul>
<hr/>
<table class="table  table-bordered"> 
    <tr  class="active" >
        <th class="" colspan="4">Chi tiết thông tin </th>
    </tr>
    <?php foreach($pointItems as $id=> $pointItem){ ?> 
        <tr>
            <th>
                <?php if($pointItem['PointItem']['method']=="Bán") {?> 
                    <strong>Bán</strong> 
                <?php }else{ ?>
                    <strong>Cho</strong> 
                <?php } ?>
            </th>
            <td colspan="3"><?php echo $pointItem['PointItem']['product_name'];?></td>
        </tr>
        
        <?php if($pointItem['PointItem']['method']=="Bán") {?> 
            <tr>
                <th>Giá bán </th>
                <td ><?php echo @number_format($pointItem['PointItem']['price']);?></td>
                <th>Số lượng </th>
                <td ><?php echo @number_format($pointItem['PointItem']['price']);?></td>
            </tr>
        <?php } else { ?>
            <tr>
                <th>Số lượng </th>
                <td colspan="3" ><?php echo @number_format($pointItem['PointItem']['amount']);?></td>
            </tr>
        <?php } ?>

        <?php if($pointItem['PointItem']['start_date']!="" && $pointItem['PointItem']['end_date']!=""){ ?>
            <tr>
                <th>Thời gian </th>
                <td colspan="3">
                Từ <?php echo date('d-m-Y', strtotime($pointItem['PointItem']['start_date']) );?> đến <?php echo date('d-m-Y', strtotime($pointItem['PointItem']['end_date']) );?>
                </td>            
            </tr>
        <?php } else { 
            if($pointItem['PointItem']['schedule']!=""){ ?>
            <tr>
                <th>Thời gian </th>
                <td colspan="3">
                <?php echo $pointItem['PointItem']['schedule'];?>
                </td>
            </tr>
            <?php } ?>
        <?php } ?>
        <?php if($pointItem['PointItem']['desc'] != ''){ ?>
            <tr>
                <th>Ghi chú </th>
                <td colspan="3"><?php echo $pointItem['PointItem']['desc'];?></td>
            </tr>
        <?php } ?>
        <tr class="active">
            <td colspan=4></td>
        </tr>
    <?php } ?>
        <tr>
            <td colspan="4">
                <button 
                    type="button" 
                    class="btn btn-primary" 
                    href="javascript:void(0)" 
                    ref="enroll"
                    order-id="<?php echo $point['Point']['id']; ?>"> Đăng ký thông tin</button>
            </td>
        </tr>
</table>




