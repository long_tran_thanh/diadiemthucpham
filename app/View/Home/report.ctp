<meta http-equiv="refresh" content="10;url=<?php echo SERVER; ?>"/>

<div class="container mt-5">
    <div class="col-md-12 ">
        <div class="panel panel-success mt-5 ">
            <div class="panel-heading">Thông tin đơn vị, cá nhân cung cấp thực phẩm </div>
            <div class="panel-body">
                <p>
                    Chúng tôi đã nhận được thông tin về đơn hàng bạn gởi, trong thời gian sớm nhất chúng tôi sẽ liên hệ với bạn. Xin cám ơn, Hệ thống sẽ tự quay trở về trang chủ sau 10 giây nữa
                </p>                   
                <a href="/" class="text-center btn btn-primary">Quay lại trang chủ<a>
            </div>
        </div>
    </div>
</div>   