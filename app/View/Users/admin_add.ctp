<div class="col-md-6">
	<?php echo $this->Form->create('User'); ?>

	<div class="form-group">
		<label for="exampleInputEmail1">Email</label>
		<input name="data[User][email]" type="text" id="UserEmail" class="form-control" required="required">
	</div>

	<div class="form-group">
		<label for="exampleInputEmail1">Nhập mật khẩu</label>
		<input name="data[User][password]" type="password" id="UserPassword" class="form-control" required="required">
		<small>Mật khẩu phải có ít nhất 6 ký tự</small>
	</div>

	<div class="form-group">
		<label for="exampleInputEmail1">Nhóm sử dụng</label>
		<select name="data[User][role]" id="UserRole" class="form-control" required="required">
			<option value="manager">Tổng tập viên</option>
			<option value="mod">Biên tập viên</option>
			<option value="user">Phóng viên </option>
		</select>
	</div>
	<button id="btnUpdateEmail" type="submit" name="btnUpdate" class="btn btn-danger">Cập nhật Email</button>
	</form>
</div>


<script>
$(document).ready(function(){
	$()
    $('#btnUpdatePassword').click(function(){
        if($('#UserEmail').val() == '' || $('#UserPassword').val() == '' || $('#UserPassword').val().length < 6 ){
            alert('Thiếu thông tin đăng nhập và mật khẩu nhỏ hơn 6 ký tự')
            return false;
        }
        return true;

    });
});
</script>