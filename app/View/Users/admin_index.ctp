<?php $userCurrentLogin = $this->Session->read(SESSION_ADMIN_DATA);?>

<div class='clearfix'></div>
<?php if($userCurrentLogin['User']['role']==='admin' || $userCurrentLogin['User']['role']==='manager'){ ?>
<div class="row" >
	<div class="col-md-12 pull-right"  >
		<a class='btn btn-success pull-right' data-toggle="modal" data-target="#addUserModal"> Thêm mới dữ liệu </a>
	</div>
</div>
<?php } ?>

<br/>

<?php if(isset($validationErrors)){?>
	<ul class="text-danger">
	<?php foreach($validationErrors as $field => $messages){?>
		<?php foreach($messages as $i => $text){?>
			<li><?php echo $text;?></li>
		<?php } ?>
	<?php } ?>
	</ul>
<?php }?>

<table class='table table-bordered ' >
	<tr>
		<th><?php echo $this->Paginator->sort('id', '#');?></th>
		<th><?php echo $this->Paginator->sort('email', 'Email đăng nhập');?></th>
		<th><?php echo $this->Paginator->sort('email', 'Họ tên');?></th>
		<th><?php echo $this->Paginator->sort('role', 'Quyền');?></th>
		<th><?php echo $this->Paginator->sort('created', 'Ngày khởi tạo');?></th>
		<th>Cập nhật</th>
	</tr>
	<?php $inc = 1;?>
	<?php foreach ($userData as $key => $user){?>

	<tr rel='data'>
		<td>
		<?php echo $inc++ ?>
		</td>
		<td><?= $user['User']['email'];?></td>
		<td><?= $user['User']['name'];?></td>
		<td><?= $user['User']['role'];?></td>
		<td><?php echo date('d-m-Y H:i', strtotime($user['User']['created'])); ?></td>
		<td >
			<?php if(in_array($userCurrentLogin['User']['role'], array('admin','manager')) || $user['User']['email']===$userCurrentLogin['User']['email']){ ?>
				<a href="/admin/users/changepassword/<?php echo $user['User']['id']; ?>">Thay đổi mật khẩu</a> |
				<a href="/admin/users/change_email/<?php echo $user['User']['id']; ?>">Thay đổi email</a> |
				<a href="/admin/users/delete_account/<?php echo $user['User']['id'];?>"> Xóa </a>
			<?php } ?>
		</td>
	</tr>
<?php
}?>
</table>

<?php if((int)$this->Paginator->counter('{:pages}')>1){ ?>
<div class='pagination pull-right'>
	<ul class='pagination pagination-sm'>
	<?php
		echo $this->Paginator->first('<<',array('tag' => 'li'));
		echo $this->Paginator->prev('<', array('tag' => 'li'), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a'));
		echo $this->Paginator->numbers(array('separator' => '','currentTag' => 'a', 'currentClass' => 'active','tag' => 'li','first' => 1));
		echo $this->Paginator->next('>', array('tag' => 'li','currentClass' => 'disabled'), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a'));
		echo $this->Paginator->last('>>',array('tag' => 'li'));
	?>
	</ul>
	<p><small class='pull-right'>
		<?php echo $this->Paginator->counter('Trang số {:page} trong {:pages} trang, hiển thị {:current} dòng trong tổng số {:count} dòng '); ?></small>
	</p>
</div>
<?php }?>



<div class="modal" id="addUserModal" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Thông tin người dùng </h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
	  <form action="/admin/users/add" id="UserAdminIndexForm" method="post" accept-charset="utf-8">
      <div class="modal-body">


			<div class="form-group">
				<label for="exampleInputEmail1">Email</label>
				<input name="data[User][email]" type="text" id="UserEmail" class="form-control" required="required">
			</div>

			<div class="form-group">
				<label for="exampleInputEmail1">Họ và tên</label>
				<input name="data[User][name]" type="text" id="UserName" class="form-control" required="required">
			</div>

			<div class="form-group">
				<label for="exampleInputEmail1">Nhập mật khẩu</label>
				<input name="data[User][password]" type="password" id="UserPassword" class="form-control" required="required">
				<small>Mật khẩu phải có ít nhất 6 ký tự</small>
			</div>

			<div class="form-group">
				<label for="exampleInputEmail1">Nhóm sử dụng</label>
				<select name="data[User][role]" id="UserRole" class="form-control" required="required">
					<option value="manager">Trưởng phòng tập viên</option>
					<option value="mod">Biên tập viên</option>
					<option value="user">Phóng viên </option>
				</select>
			</div>
		</div>
      <div class="modal-footer">
        <button type="submit" id="btnUpdatePassword" class="btn btn-success">Cập nhật </button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Đóng </button>
      </div>
	  </form>
    </div>
  </div>
</div>


<script>
$(document).ready(function(){
    $('#btnUpdatePassword').click(function(){
		if($('#UserEmail').val() == '' || $('#UserPassword').val() == '' || $('#UserPassword').val().length < 6 ){
            alert('Thiếu thông tin đăng nhập và mật khẩu nhỏ hơn 6 ký tự')
            return false;
        }

        return true
    });
});
</script>