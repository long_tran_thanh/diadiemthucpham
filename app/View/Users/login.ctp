<br/>

<style>

.main-head{
    height: 150px;
    background: #FFF;

}

.sidenav {
    height: 100%;
    background-color: #000;
    overflow-x: hidden;
    padding-top: 20px;
}


.main {
    padding: 0px 10px;
}

@media screen and (max-height: 450px) {
    .sidenav {padding-top: 15px;}
}

@media screen and (max-width: 450px) {
    .login-form{
        margin-top: 10%;
    }

    .register-form{
        margin-top: 10%;
    }
}

@media screen and (min-width: 768px){
    .main{
        margin-left: 20%;
    }

    .sidenav{
        width: 20%;
        position: fixed;
        z-index: 1;
        top: 0;
        left: 0;
    }

    .login-form{
        margin-top: 20%;
    }

    .register-form{
        margin-top: 20%;
    }
}


.login-main-text{
    margin-top: 20%;
}

.login-main-text h2{
    font-weight: 300;
}

.btn-black{
    background-color: #000 !important;
    color: #fff;
}
</style>

<div class="sidenav">
</div>
<div class="main">
    <div class="col-md-6 col-sm-12">
    <div class="login-form">
    <div class="login-main-text">
        <h2 class="text-success text-uppercase">Đăng nhập!</h2>
        <p class="text-bold"><strong>Đăng nhập hệ thống quản lý nội dung </strong></p>
    </div>
        <?php echo $this->Session->flash('auth'); ?>
        <form id="loginform" role="form" accept-charset="utf-8" method="post" action="/users/login">
            <div class="form-group">
                <label>Email</label>
                <input class="form-control" placeholder="E-mail" name="user[email]" type="email" autofocus>
            </div>
            <div class="form-group">
                <label>Password</label>
                <input class="form-control" placeholder="Password" name="user[password]" type="password" value="">
            </div>
            <button type="submit" class="btn btn-black">Đăng nhập!</button>
        </form>
    </div>
    </div>
</div>

