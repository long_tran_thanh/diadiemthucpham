<div class="container">
    <div class="row">
    <!-- Latest Posts -->
    <main class="post blog-post col-lg-8">
        <div class="container">
        <div class="post-single">

            <div class="post-details">
            <h1><?php echo $page['Page']['title'];?></h1>
            <div class="post-body">
            <?php echo $page['Page']['description'];?>
            </div>
            </div>
        </div>
        </div>
    </main>
    <aside class="col-lg-4">
        <div class="row item">
        <a href="<?php echo @$websiteInfo['link_adv_1'];?>" class="pb-2">
            <img src="<?php echo @$websiteInfo['img_adv_1'];?>" class="img-fluid" alt="">
        </a>

        <a href="<?php echo @$websiteInfo['link_adv_2'];?>" class="pb-2">
            <img src="<?php echo @$websiteInfo['img_adv_2'];?>" class="img-fluid" alt="">
        </a>
        </div>
    </aside>
    </div>
</div>
