<link rel="stylesheet" href="/css/recruitment.css">


<div class="row main">
  <div class="inmain">
    <div class="tuyen-dung">
      <img src="/images/tuyen-dung-banner.jpg" alt="">
      <div class="main-tuyen-dung">
        <h4>THÔNG TIN TUYỂN DỤNG</h4>
        <p>Nhằm mở rộng và nâng cao chất lượng dịch vụ, chúng tôi cần tuyển dụng các vị trí sau:</p>
        <?php echo $page['Page']['description'];?>

        <p><strong>Lưu ý:</strong> Vui lòng ghi rõ chức danh công việc dự tuyển, số điện thoại liên hệ lên bìa hồ sơ xin việc. Hồ sơ đạt yêu cầu Công ty sẽ liên hệ lại qua điện thoại. Hồ sơ không trúng tuyển công ty sẽ không hoàn trả lại.</p>
        <h5>ỨNG VIÊN NỘP HỒ SƠ TẠI CÔNG TY TNHH THƯƠNG MẠI DỊCH VỤ VẬN TẢI ĐÔNG HƯNG</h5>

        <div class="address">
          <h5>Tại TPHCM:</h5>
          <p><i class="fas fa-map-marker-alt"></i>Địa chỉ: <?php echo $websiteInfo['saigon_address'];?></p>
          <p><i class="fas fa-phone"></i>Điện thoại: <?php echo $websiteInfo['saigon_mobile_address'];?> - Fax: <?php echo $websiteInfo['saigon_fax_address'];?></p>

          <h5>Tại Bình Thuận ( Bến xe Đông Hưng - Phan Rí Cửa ):</h5>
          <p><i class="fas fa-map-marker-alt"></i>Địa chỉ: <?php echo $websiteInfo['phanricua_address'];?></p>
          <p><i class="fas fa-phone"></i>Hotline: <?php echo $websiteInfo['phanricua_mobile_address'];?></p>

        </div>
      </div>
    </div>


    <?php echo $this->element('blocks-services');?>
    <?php echo $this->element('blocks-advs');?>

    <div class="news">
        <div class="tic">TIN TỨC</div>
        <img src="/images/home/bg-news.jpg" alt="<?php echo $websiteInfo['title'];?>">
        <div class="des-news">
        <?php foreach($blogs as $id=>$blog){?>
        <div class="des-news-item">
            <p class="time-des"><?php echo date('H:i d-m-Y', strtotime($blog['Blog']['updated']));?></p>
            <p class="header-des"><a href="tin-tuc/<?php echo $blog['Blog']['id'];?>/<?php echo _rename($blog['Blog']['title']);?>.html"><?php echo $blog['Blog']['title'];?></a></p>
            <p class="main-des"><?php echo $this->App->getShortDescription($blog['Blog']['description']);?>
            <span><a href="/tin-tuc/<?php echo $blog['Blog']['id'];?>/<?php echo _rename($blog['Blog']['title']);?>.html">Xem thêm</a></span>
            </p>
        </div>
        <?php } ?>
        </div>
    </div>




  </div>
</div>


