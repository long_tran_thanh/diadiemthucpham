<link rel="stylesheet" href="/css/service.css">



<div class="service">
	<?php $sliders = $contentPage['PageImage'];?>
  <?php if(!empty($sliders)){ ?>
      <div id="demo" class="carousel slide" data-ride="carousel">
          <!-- Indicators -->
          <ul class="carousel-indicators">
          <?php foreach($sliders as $key => $slider){ ?>
          <li data-target="#demo" data-slide-to="<?php echo $key;?>" <?php echo ($key == 0? 'class="active"':""); ?>></li>
          <?php } ?>
          </ul>

          <!-- The slideshow -->
          <div class="carousel-inner">
          <?php foreach($sliders as $key => $slider){ ?>
          <div class="carousel-item <?php echo ($key == 0? "active":""); ?>">
              <img src="<?php echo $slider['url']; ?>" alt="" class="img-fluid">
          </div>
          <?php }?>
          </div>

          <!-- Left and right controls -->
          <a class="carousel-control-prev" href="#demo" data-slide="prev">
          <span class="carousel-control-prev-icon"></span>
          </a>
          <a class="carousel-control-next" href="#demo" data-slide="next">
          <span class="carousel-control-next-icon"></span>
          </a>
      </div>
  <?php } ?>

  <?php if($id == 2 || $id == 1){ ?>
  <link rel="stylesheet" href="/css/index.css">
  <style>
  body .book .right-book .main-right-book {
      border: 1px solid #80492b;
      border-radius: 2px;
      padding: 35px;
      padding-bottom: 22px;
      padding-top: 20px;
      margin-top: -18px;
      margin-left: -10px;
  }
  .btn-book {
      background-color: #6b1814;
      color: #fff;
      width: 100px;
      font-weight: bold;
      cursor: pointer;
  }
  </style>
  <div class="row book clearfix">
      <div class="col-sm-6 col-md-6 left-book">
          <div class="header-book">
          <div class="main-header-book"><p>MUA VÉ TRỰC TUYẾN</p></div>
          <div class="icon-header-book">
              <i class="fa fa-bus"></i>
          </div>
          </div>
          <div class="main-left-book">
          <form class="" action="/mua-ve-xe-p1" method="get">
              <div class="row">
              <div class="col-sm-6 col-md-6 left-item1">
                  <div class="form-group">
                  <label for="">Điểm khởi hành<i class="fa fa-exchange swap"></i></label>
                  <!--i class="fa fa-map-marker posab"></i-->
                  <select class="form-control" name="from" id="from" required>
                      <option value="Sài gòn">Sài Gòn</option>
                  </select>
                  </div>
                  <div class="form-group clock">
                  <label for="">Ngày khởi hành</label>
                  <img src="/images/uploads/clock.png" class="posab">
                  <input min="<?php echo date('d-m-Y');?>" required="required" class="form-control date" type="date" name="date" value="<?php echo date('m/d/Y');?>">
                  </div>
              </div>
              <div class="col-sm-6 col-md-6 left-item2">
                  <div class="form-group">
                  <label for="">Điểm đến</label>
                  <!--i class="fa fa-map-marker posab"></i-->
                  <select class="form-control" name="to" id="to" required>
                      <option value="Cai Lậy"> Cai Lậy </option>
                      <option value="Chợ Lách"> Chợ Lách </option>
                  </select>
                  </div>
                  <div class="form-group">
                  <label for="">Số lượng vé</label>
                  <!--i class="fa fa-ticket posab"></i-->
                  <input type="number" name="quantity" class="form-control" value="1" required="required">
                  </div>
              </div>
              </div>
              <button class="form-control btn-book" name="button">MUA VÉ</button>
          </form>
          </div>
      </div>

      <div class="col-sm-6 col-md-6 right-book">
          <div class="header-book">
          <div class="main-header-book"><p>TRA CỨU MÃ HÀNG TRỰC TUYẾN</p></div>
              <div class="icon-header-book">
                  <i class="fa fa-search"></i>
              </div>
          </div>
          <div class="main-right-book">
          <form  action="/tim-kiem/" method="get">
              <div class="form-group">
                  <label for="">Nhập mã đơn hàng </label>
                  <input class="form-control" required="required" type="text" name="phieu_code" value="" maxlength="8" placeholder="Nhập mã đơn hàng">
              </div>

              <div class=" form-group">
                  <label for="">Số điện thoại người nhận </label>
                  <input class="form-control" required="required" type="text" name="phieu_sdtnhan" value="" maxlength="12" placeholder="">
              </div>

              <button class="form-control btn-book" >TRA CỨU</button>
          </form>
          </div>
      </div>
  </div>
  <?php }?>


	<div class="des-service">
    <?php echo $contentPage['Page']['description'];?>
		<div class="p3-des">
      <h3>Các địa điểm đặt vé:</h3>
      <div class="des-address">
        <p class="p1">Trạm Sài Gòn:</p>
        <p class="p2"><i class="fa fa-map-marker"></i><?php echo @$websiteInfo['saigon_address'] ?></p>
        <p class="p2"><i class="fa fa-phone"></i><?php echo @$websiteInfo['saigon_mobile_address'] ?></p>
      </div>
      <div class="des-address">
        <p class="p1">Trạm Chợ Lách:</p>
        <p class="p2"><i class="fa fa-map-marker"></i><?php echo @$websiteInfo['cholach_address'] ?></p>
        <p class="p2"><i class="fa fa-phone"></i><?php echo @$websiteInfo['cholach_mobile_address'] ?></p>
      </div>
      <div class="des-address">
        <p class="p1">Trạm Cai Lậy:</p>
        <p class="p2"><i class="fa fa-map-marker"></i><?php echo @$websiteInfo['cailay_address'] ?></p>
        <p class="p2"><i class="fa fa-phone"></i><?php echo @$websiteInfo['cailay_mobile_address'] ?></p>
      </div>
      <a href="<?php echo @$websiteInfo['banner_adv_service_link_right'] ?>"><img src="<?php echo @$websiteInfo['banner_adv_service_right'] ?>" alt=""></a>
    </div>
  </div>

  <div class="service-bottom">
  <div class="tic">DỊCH VỤ</div>
  <div class="main-service-bottom">
    <div class="row inmain-service-bottom">
      <?php foreach($services as $id => $service ){?>
      <div class="col-sm-2 col-md-2 service-bottom-item">
        <a href="/thong-tin/<?php echo $service['Page']['id']; ?>-<?php echo $service['Page']['key'];?>.html">
          <img src="<?php echo $service['Page']['images'];?>" alt="<?php echo $service['Page']['title'];?>">
          </a>
        <p><?php echo $service['Page']['title'];?></p>
      </div>
      <?php }?>

    </div>
  </div>
  </div>

</div>
