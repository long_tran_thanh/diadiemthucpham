<link rel="stylesheet" href="/css/schedule.css">
<div class="row main">
    <div class="inmain">
        <div class="lich-trinh">
            <?php $title = "";?>
            <?php foreach($roads as $c => $road){?>
                <?php if($title != $road['Road']['title']){ ?>
                    <?php $title = $road['Road']['title'];?>
                    <div class="table-responsive item">
                        <h3><i class="fas fa-bus red"></i><?php echo $title;?></h3>
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>STT</th>
                                    <th>Bến đi</th>
                                    <th>Bến đến</th>
                                    <th>Loại xe</th>
                                    <th>Giá vé</th>
                                    <th>Giờ chạy</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                <?php } ?>
                    <tr>
                        <td><?php echo $c+1 ?></td>
                        <td><?php echo $road['Road']['from'];?></td>
                        <td><?php echo $road['Road']['to'];?></td>
                        <td class="strong red"><?php echo $trip_car_type[$road['Road']['trip_car_type']];?></td>
                        <td class="strong red"><?php echo number_format($road['Road']['price']);?>đ/vé</td>
                        <td class="strong"><?php echo $road['Road']['start_time'];?></td>
                        <td><a href="javascript:void(0)" ref="booking" start="<?php echo $road['Road']['from'];?>" to="<?php echo $road['Road']['to'];?>"><i class="fas fa-ticket-alt"></i>Mua vé</a></td>
                    </tr>

                <?php if(!isset($roads[$c+1]['Road']['title']) || $title != $roads[$c+1]['Road']['title']) { ?>
                            </tbody>
                        </table>
                    </div>
                <?php } ?>

            <?php }?>



            <?php echo $this->element('blocks-services');?>
            <?php echo $this->element('blocks-advs');?>

            <div class="news">
                <div class="tic">TIN TỨC</div>
                <img src="/images/home/bg-news.jpg" alt="<?php echo $websiteInfo['title'];?>">
                <div class="des-news">
                <?php foreach($blogs as $id=>$blog){?>
                <div class="des-news-item">
                    <p class="time-des"><?php echo date('H:i d-m-Y', strtotime($blog['Blog']['updated']));?></p>
                    <p class="header-des"><a href="tin-tuc/<?php echo $blog['Blog']['id'];?>/<?php echo _rename($blog['Blog']['title']);?>.html"><?php echo $blog['Blog']['title'];?></a></p>
                    <p class="main-des"><?php echo $this->App->getShortDescription($blog['Blog']['description']);?>
                    <span><a href="/tin-tuc/<?php echo $blog['Blog']['id'];?>/<?php echo _rename($blog['Blog']['title']);?>.html">Xem thêm</a></span>
                    </p>
                </div>
                <?php } ?>
                </div>
            </div>

        </div>
    </div>
</div>



<div class="modal fade" id="frm-booking" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Đặt vé trực tuyến! </h4>
        </div>
        <div class="modal-body">
			<form class="" action="/mua-ve-xe-p1" method="GET" >
			<div class="row">
				<div class="col-sm-6 col-md-6 ">
					<div class="form-group">
						<label for="">Điểm khởi hành</label>
						<select class="form-control" name="from" id="from" required="required">

						</select>
					</div>
					<div class="form-group clock">
						<label for="">Ngày khởi hành</label>
						<input min="02-03-2019" required="required" class="form-control date" type="date" name="date" value="10/08/2019">
					</div>
				</div>
				<div class="col-sm-6 col-md-6 ">
					<div class="form-group">
						<label for="">Điểm đến</label>
						<select class="form-control" name="to" id="to" required="required">
						</select>
					</div>
					<div class="form-group">
						<label for="">Số lượng vé</label>
						<input type="number" min="1" max="45" class="form-control" value="1" name="quantity">
					</div>
				</div>
				<div class="col-sm-12 col-md-12 ">
					<div class="float-right">
						<button class="btn btn-book btn-danger" name="button">MUA VÉ</button>
						<button class="btn btn-primary" data-dismiss="modal">Đóng </button>
					</div>
				</div>
			</div>
			</form>
        </div>
      </div>
    </div>
  </div>

  <script>
	$(document).ready(function(){
		$('a[ref=booking]').click(function(){
			var start=$(this).attr('start')
			var to=$(this).attr('to')
			$('#from').html('<option value="'+ start +'">'+start+'</option>')
			$('#to').html('<option value="'+ to +'">'+to+'</option>')

			$('#frm-booking').modal();
		})
	});

</script>


