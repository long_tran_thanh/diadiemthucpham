<?php
/**
 * Application level View Helper
 *
 * This file is application-wide helper file. You can put all
 * application-wide helper-related methods here.
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.View.Helper
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses('Helper', 'View');

/**
 * Application helper
 *
 * Add your application-wide methods in the class below, your helpers
 * will inherit them.
 *
 * @package       app.View.Helper
 */
class AppHelper extends Helper {

	var $helpers = array ('Html', 'Form', 'Session');

	/**
	 * [displayValidationError description]
	 * @param  [type] $validationErrors [description]
	 * @param  [type] $key              [description]
	 * @return [type]                   [description]
	 */
	function displayValidationError($validationErrors = null, $key = null){
		if($validationErrors == null){
			return null;
		}

		if(!empty($validationErrors[$key])){
			return '<p class="text-danger">'. implode('<br> - ', $validationErrors[$key]) .'</p>';
		}
	}

	/**
	 * [loadFancyBox description]
	 * @return [type] [description]
	 */
	function loadFancyBox(){
		$libraries = array(
			'<link rel="stylesheet" href="/css/jquery.fancybox.css">',
			'<script src="/js/jquery.fancybox.js"></script>'
		);

		return implode('', $libraries);
	}

	/**
	 * [createEditor description]
	 * @return [type] [description]
	 */
	function createEditor(){
		$libraries = array(
			'<script src="/js/ckeditor/ckeditor.js"></script>'
		);

		return implode('', $libraries)	;
	}

	/**
	 * [RecursiveCategories description]
	 * @param [type] $array [description]
	 */
	function recursiveCategories($categories = null, $count = null) {
	    if (count($categories)) {
	        echo "\n<ul ". ($count==null?"class=\"list-group\"":'' ) .">\n";
	        $count = empty($count) ? $count : 1;
	        foreach ($categories as $category) {

	            echo "<li  id=\"".$category['Category']['id']."\">".$category['Category']['name'];
	            if (count($category['children'])) {
	                $this->recursiveCategories($category['children'], $count);
	            }
	            echo "</li>\n";
	        }
	        echo "</ul>\n";
	    }
	}

	/**
	 * [selectCategory description]
	 * @param  [type] $categoryId [description]
	 * @return [type]             [description]
	 */
	function selectCategory($categoryList = null, $categoryIdSelected = null, $options = null){
		if($categoryList == null){
			return null;
		}

		$resp = '<select class="form-control" name="'.$options['name'] .'">';
		$resp .= '<option value="1" '. ($categoryIdSelected == 1?"selected='Selected'":"") .' >-- Danh mục gốc ---</option>';

		foreach ($categoryList as $categoryTempId => $categoryTitle){
			if( $categoryIdSelected == $categoryTempId){
				$resp .='<option selected="selected" value="'. $categoryTempId .'">'.$categoryTitle .'</option>';
			}else{
				$resp .='<option value="'. $categoryTempId .'">'.$categoryTitle .'</option>';
			}
	 	}

		$resp .= '</select>';
		return $resp;
	}

	/**
	 *
	 * @param  [type] $str [description]
	 * @return [type]      [description]
	 */
	function slug($string = null){

		$slug=preg_replace('/[^A-Za-z0-9-]+/', '-', strtolower($string));
   		return $slug;
	}

	/**
	 *
	 * @param  [type] $slider [description]
	 * @param  [type] $image  [description]
	 * @return [type]         [description]
	 */
	function getTextSlider($sliders = null, $image = null){
		if($sliders == null || $image == null){
			return null;
		}
		foreach ($sliders as $key => $slider) {
			if($image == $slider['Slider']['image']){
				return '<input type="text" placeholder="Link khi click" class="form-control" name="slider_'. $slider['Slider']['id'].'" data-type="text1" data-id="'.$slider['Slider']['id'].'" rel="slider_text" value="'. $slider['Slider']['text1']  .'"> <br/>';
			}
		}
	}

	/**
	 * [getTextSlider description]
	 * @param  [type] $sliders [description]
	 * @param  [type] $image   [description]
	 * @return [type]          [description]
	 */
	function getTextForSlider($sliders = null, $image = null, $field = null){
		if($sliders == null || $image == null ||  $field == null){
			return null;
		}

		foreach ($sliders as $key => $slider) {
			if($image == $slider['Slider']['image']){
				return  isset($slider['Slider'][$field]) ? $slider['Slider'][$field] : null;
			}
		}
	}

	/**
	 * [contentPage description]
	 * @param  [type] $key [description]
	 * @return [type]      [description]
	 */
	public function contentPage($key = null, $field = 'description'){
		App::import('Model', 'Page');
		$this->Page = new Page();

		$page = $this->Page->findByKey($key);
		if(empty($page)){
			return null;
		}

		if($key!=null && isset($page['Page'][$field])){
			return $page['Page'][$field];
		}else{
			return $page['Page'];
		}
	}

	/**
	 * [contentEmbed description]
	 * @param  [type] $key [description]
	 * @return [type]      [description]
	 */
	public function contentEmbed($key, $caseResp = false){
		App::import('Model', 'Page');
		$this->Page = new Page();

		$page = $this->Page->findByKey($key);
		if(empty($page)){
			return null;
		}

		switch($caseResp){
			case 'youtube':
				$youTubeId = str_replace('https://www.youtube.com/embed/','',$page['Page']['link']);
				$youTubeId = str_replace('/','',$youTubeId);
				return $youTubeId;
			break;
		}
		return $page['Page']['link'];
	}

	public function getYoutubeId($youtubeUrl = none){

		$youTubeId = str_replace('https://www.youtube.com/watch?v=','',$youtubeUrl);
		$youTubeId = str_replace('/','',trim($youTubeId));

		return $youTubeId;
	}


	/**
	 * [contentImage description]
	 * @param  [type]  $key    [description]
	 * @param  integer $number [description]
	 * @return [type]          [description]
	 */
	public function contentImage($key, $number = 0){
		App::import('Model', 'Page');
		$this->Page = new Page();

		$page = $this->Page->findByKey($key);
		if(empty($page)){
			return null;
		}

		$images = explode(";", $page['Page']['images']);
		return (isset($images[$number]) ? $images[$number] : null);
	}

	/**
	 * [getDataCustomer description]
	 * @param  [type] $customerData [description]
	 * @param  [type] $image        [description]
	 * @return [type]               [description]
	 */
	function getDataCustomer($customerData = null, $image = null){
		if($customerData == null || $image == null){
			return null;
		}

		foreach ($customerData as $customer) {
			if($image == $customer['Customer']['logo']) {
				return $customer;
			}
		}

		return null;
	}

	/**
	 * [getShortDescription description]
	 * @param  [type] $words [description]
	 * @return [type]        [description]
	 */
	function getShortDescription($words = null, $stripTags = true){
		if($words == null){
			return null;
		}

		$description = explode(EXPLODE_BLOG, $words);
		return ($stripTags == true ? strip_tags($description[0]) : $description[0] ) ;
	}



	public function findKeyInPage($groupId = null, $key = null){
		App::import('Model', 'Page');
		$this->Page = new Page();

		return $this->Page->find('first', array('conditions'=>array(
					'group'=>$groupId,
					'key'=> $key
				)
			)
		);

	}

	public function createBlankPage($groupId = null, $key = null){
		App::import('Model', 'Page');
		$this->Page = new Page();

		$pageData['Page']['key']  = trim($key);
		$pageData['Page']['title']  = trim($key);
		$pageData['Page']['group']  = $groupId;
		$pageData['Page']['description']  = null;

		$this->Page->set($pageData);
		$this->Page->save();
	}



	public function findByCategoryKey($pages, $key = null, $field = null){
		if (empty($pages)){
			return null;
		}

		foreach($pages as $page){
			if ($page['Page']['key'] == $key){
				if(isset($page['Page'][$field])){
					return $page['Page'][$field];
				}else{
					return $page;
				}

			}
		}

		return null;
	}


	public function findByCategoryKeyList($pages, $key = null){
		if (empty($pages)){
			return null;
		}
		App::import('Model', 'Category');
		$this->Category = new Category();
		$pageGroup = $this->Category->findByKey($key);
		if($pageGroup == null){
			return null;
		}

		$resp = null;
		foreach($pages as $page){
			if ($page['Page']['group'] == $pageGroup['Category']['id']){
				$resp[] =  $page;
			}
		}

		return $resp;
	}



	public function getChildrenCategory($groupId = null){
		App::import('Model', 'Category');
		$this->Category = new Category();

		$page = $this->Category->find('list', array(
				'conditions'=>array(
					'parent_id'=>$groupId,
				),
				'order'=>'ord ASC'
			)
		);

		return $page;
	}


	public function getUserManagementRole(){
		$userCurrentLogin = $this->Session->read(SESSION_ADMIN_DATA);
		if(empty($userCurrentLogin)){
			return False;
		}

		$role = $userCurrentLogin['User']['role'];
		$managementUserRole = getUserManagementRole();
		if(!in_array($role, $managementUserRole)) {
			return False;
		}

		return True;
	}

}
