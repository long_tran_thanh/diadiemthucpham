<div class="container">
      <div class="row">
        <!-- Latest Posts -->
        <main class="posts-listing col-lg-8">
          <div class="container">
            <div class="row">

              <h3 class="h6">Tìm kiếm: <?php echo $title_search;?> </h3>
              <div class="clearfix"></div>
              <?php if(empty($blogDatas)){?>
                <div class="alert alert-danger" role="alert">
                  Không tìm thấy dữ liệu
                </div>
              <?php } ?>
              <?php foreach($blogDatas as $key => $blog){?>
                <div class="clearfix"></div>

                <div class="row">
                  <div class="col-md-3">
                    <img src="<?php echo $blog['BlogImage'][0]['url'];?>" class="img-fluid"  alt="<?php echo $blog['Blog']['title'];?>">
                  </div>

                  <div class="col-md-9">
                    <a href="<?php echo SERVER;?>/tin-tuc/<?php echo $blog['Blog']['id'];?>/<?php echo _rename($blog['Blog']['title']) ?>.html">
                      <?php echo $blog['Blog']['title'];?>
                    </a>
                    <p><?php echo $blog['Blog']['short_description'];?></p>
                  </div>
                </div>
              <?php }?>

            </div>
          </div>
        </main>

        <aside class="col-lg-4">
          <div class=" latest-posts">
            <div class="blog-posts">
              <?php foreach($blogList as $key => $blog){?>
                <div class="item mb-3">
                    <?php if($key==0){ ?>
                      <img src="<?php echo $blog['BlogImage'][0]['url'];?>" class="img-fluid" alt="<?php echo $blog['Blog']['title'];?>">
                      <div class="cleanfix"></div>
                    <?php }?>

                    <div class="title">
                      <a href="<?php echo SERVER;?>/tin-tuc/<?php echo $blog['Blog']['id'];?>/<?php echo _rename($blog['Blog']['title']) ?>.html">
                        <?php echo $blog['Blog']['title'];?>
                      </a>
                    </div>
                    <div class="short_description">
                    <?php echo $blog['Blog']['short_description'];?>
                    </div>
                </div>
              <?php }?>
            </div>
          </div>

        </aside>
      </div>
    </div>