<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title><?php echo isset($blogInfo['Blog']['title']) ? $blogInfo['Blog']['title'] : @$websiteInfo['title']  ;?> </title>
    <meta name="description" content="<?php echo @$websiteInfo['meta_description'];?>">
    <meta name="keywords" content="<?php echo @$websiteInfo['meta_tag_keyword'];?>">
    <meta name="author" content="<?php echo @$websiteInfo['meta_author'];?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="robots" content="all,follow">
    <meta property="og:title" content="<?php echo isset($blogInfo['Blog']['title']) ? $blogInfo['Blog']['title'] : @$websiteInfo['title']  ;?>" />
    <meta property="og:description" content="<?php echo isset($blogInfo['Blog']['short_description']) ? $blogInfo['Blog']['short_description'] : @$websiteInfo['meta_description'];?>" />
    <meta property="og:image" content="<?php echo isset($blogInfo['BlogImage'][0]['url']) ? SERVER.$blogInfo['BlogImage'][0]['url'] : @$websiteInfo['logo'];?>" />
    <meta prooerty="og:url" content="<?php echo SERVER.$this->request->here; ?>"/>
    <meta prooerty="og:type" content="website"/>
    <meta prooerty="og:locate" content="vi"/>

    <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
    <link rel="manifest" href="/site.webmanifest">

    <link rel="stylesheet" href="<?php echo SERVER;?>vendor/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo SERVER;?>vendor/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="<?php echo SERVER;?>css/fontastic.css">

    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,700">
    <link rel="stylesheet" href="<?php echo SERVER;?>vendor/@fancyapps/fancybox/jquery.fancybox.min.css">
    <link type="text/css" rel="stylesheet" href="<?php echo SERVER;?>/css/lightslider.css" />
    <link rel="stylesheet" href="<?php echo SERVER;?>css/style.default.css?rand=<?php echo rand(1000,9999);?>" id="theme-stylesheet">
    <link rel="stylesheet" href="<?php echo SERVER;?>css/style.blue.css?rand=<?php echo rand(1000,9999);?>">
    <link rel="stylesheet" href="<?php echo SERVER;?>css/custom.css?rand=<?php echo rand(1000,9999);?>">
    <link rel="shortcut icon" href="favicon.png">
    <!-- Tweaks for older IEs--><!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script><![endif]-->
    <script src="<?php echo SERVER;?>/vendor/jquery/jquery.min.js"></script>

  </head>
  <body class="f8f9fa">
    <header class="header f8f9fa" >
      <!-- Main Navbar-->
      <nav class="navbar navbar-expand-lg">
        <div class="search-area">
          <div class="search-area-inner d-flex align-items-center justify-content-center">
            <div class="close-btn"><i class="icon-close"></i></div>
            <div class="row d-flex justify-content-center">
              <div class="col-md-8">
                <form action="#">
                  <div class="form-group">
                    <input type="search" name="search" id="search" placeholder="What are you looking for?">
                    <button type="submit" class="submit"><i class="icon-search-1"></i></button>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>

        <div class="container">
          <div class="row">
            <?php if(isset($is_mobile) && $is_mobile == True){ ?>
              <style>
              nav.navbar .navbar-header {
                  width: 10%;
                  float: right;
              }
              </style>


              <div class="col-6" style="padding-left: 30px;">
                <a href="<?php echo @$websiteInfo['link_adv_top'];?>"  >
                  <img src="<?php echo @$websiteInfo['logo'];?>">
                </a>
              </div>

              <div class="col-6">
                <button type="button" 
						data-toggle="collapse"
						data-target="#navbarcollapse" 
						aria-controls="navbarcollapse" 
						aria-expanded="false" 
						aria-label="Toggle navigation"
						class="navbar-toggler mt-4 float-right">
					<span></span>
					<span></span>
					<span></span>
				</button>
              </div>

              <div class="col-12">
                <div id="navbarcollapse" class="collapse navbar-collapse">
                  <ul class="navbar-nav">
                    <?php foreach($menuHeader as $menu ){?>
                    <li class="nav-item <?php echo (!empty($menu ['children']) ?'':'' );?> " >
                      <a href="<?php echo SERVER.$menu['Category']['url'];?>" class="nav-link <?php echo (!empty($menu ['children']) ?'':'' );?> " <?php echo (!empty($menu ['children']) ?'':'' );?>>
                      <b><?php echo $menu['Category']['name'];?></b>
                      </a>
                      <?php if(isset($menu['children'])){?>
                      <ul class="list-unstyled">
                        <?php foreach($menu['children'] as $key => $menu ){?>
                        <li><a class="dropdown-item" href="<?php echo $menu['Category']['url'] ;?>"><?php echo $menu['Category']['name'] ;?> </a></li>
                        <?php }?>
                      </ul>
                      <?php }?>
                    </li>
                    <?php } ?>
                    <li class="nav-item " >
                      <a href="/lien-he.html" class="nav-link">
                      <b>LIÊN HỆ</b>
                      </a>
                    </li>
                  </ul>
                </div>
              </div>

              <div class="col-12 mt-2 mb-2">
                <a href="<?php echo @$websiteInfo['img_adv_top'];?>">
                <img src="<?php echo @$websiteInfo['img_adv_top'];?>" class="img-fluid" style="padding-left: 15px;">
                </a>
              </div>
			  
            <?php }else{?>
			
			
              <div class="col-4">
                <a href="/" class="navbar-brand">
                  <img src="<?php echo @$websiteInfo['logo'];?>">
                </a>
              </div>

              <div class="col-8">
                <a href="<?php echo @$websiteInfo['link_adv_top'];?>">
                  <img src="<?php echo @$websiteInfo['img_adv_top'];?>" class="img-fluid">
                </a>
              </div>

              <div class="col-12">
                <!-- Navbar Brand -->
                <div class="navbar-header d-flex align-items-center justify-content-between">
                  <button type="button" data-toggle="collapse" data-target="#navbarcollapse" aria-controls="navbarcollapse" aria-expanded="false" aria-label="Toggle navigation" class="navbar-toggler"><span></span><span></span><span></span></button>
                </div>
                <div class="cleanfix"></div>
                <div id="navbarcollapse" class="collapse navbar-collapse">
                  <nav class="navbar navbar-expand-sm  ">
                    <!-- Brand -->
                    <!-- Links -->
                    <ul class="navbar-nav">
                      <?php foreach($menuHeader as $menu ){?>
                      <li class="nav-item <?php echo (!empty($menu ['children']) ?'dropdown':'' );?>" >
                        <a href="<?php echo $menu['Category']['url'];?>" class="nav-link <?php echo (!empty($menu ['children']) ?'dropdown-toggle':'' );?> " <?php echo (!empty($menu ['children']) ?'data-toggle="dropdown"':'' );?>>
                        <?php echo $menu['Category']['name'];?>
                        </a>
                        <?php if(isset($menu['children'])){?>
                        <div class="dropdown-menu">
                          <?php foreach($menu['children'] as $key => $menu ){?>
                          <a class="dropdown-item" href="<?php echo $menu['Category']['url'] ;?>"><?php echo $menu['Category']['name'] ;?> </a>
                          <?php }?>
                        </div>
                        <?php }?>
                      </li>
                      <?php } ?>
                      <li class="nav-item " >
                        <a href="/lien-he.html" class="nav-link">
                        LIÊN HỆ
                        </a>
                      </li>
                    </ul>
                  </nav>
                </div>
              </div>


            <?php } ?>
      </nav>
    </header>

    <?php echo $content_for_layout;?>

    <section class="featured-posts no-padding-top mt-2 mb-3" >
      <div class="container">
        <div class="row">
            <div class="col-md-12">
            <h5 class="pt-3 text-uppercase mb-2 mt-2">
							<a href="<?php echo SERVER; ?>/tin-tuc/chuyen-muc/99/thong-tin-doanh-nghiep.html">THÔNG TIN DOANH NGHIỆP </a>
						</h5>

            <ul id="lightSlider">
              <?php foreach($advCategories as $key => $advBlog){?>
              <li>
                <img src="<?php echo $advBlog['BlogImage'][0]['url'];?>" class="img-fluid h374" alt="<?php echo $advBlog['Blog']['title'];?>">
                <a href="<?php echo SERVER;?>tin-tuc/<?php echo $advBlog['Blog']['id']. '/'. _rename(strtolower($advBlog['Blog']['title']),'-')  ; ?>.html">
                  <?php echo $advBlog['Blog']['title'];?>
                </a>
              </li>
              <?php }?>

            </ul>
            </div>
        </div>
      </div>
    </section>


    <!-- Page Footer-->
    <footer class="main-footer">
      <div class="container">
        <div class="row">
          <div class="col-md-12 text-center">
            <div class="logo">
            <img src="<?php echo @$websiteInfo['logo_footer'];?>">
            </div>
            <div class="contact-details">
              <p><?php echo @nl2br($websiteInfo['company_address']) ?> </p>
              <p><?php echo @$websiteInfo['phone'] ?></p>
              <p>Email: <a href="mailto:<?php echo @$websiteInfo['email'] ?>"><?php echo @$websiteInfo['email'] ?></a></p>
              <p><?php echo @$websiteInfo['footer_content'] ?> </p>

            </div>
          </div>

        </div>
      </div>
      <div class="copyrights">
        <div class="container">
          <div class="row">
            <div class="col-md-12">
            </div>
          </div>
        </div>
      </div>
    </footer>

    <a href="#" id="back-to-top" title="Back to top">&uarr;</a>

    <!-- JavaScript files-->
    <script src="<?php echo SERVER;?>/vendor/popper.js/umd/popper.min.js"> </script>
    <script src="<?php echo SERVER;?>/vendor/bootstrap/js/bootstrap.min.js"></script>
    <script src="<?php echo SERVER;?>/vendor/jquery.cookie/jquery.cookie.js"> </script>
    <script src="<?php echo SERVER;?>/vendor/@fancyapps/fancybox/jquery.fancybox.min.js"></script>
    <script src="<?php echo SERVER;?>/js/lightslider.js"></script>
    <script src="<?php echo SERVER;?>/js/front.js?rand=<?php echo rand(1000,9999);?>"></script>

  </body>
</html>

