<p class='pull-left'>
	<button class='btn btn-success btn-flat' onClick="document.location='/admin/categories/add'">Thêm mới danh mục </button>
</p>
<table class="table table-bordered table-hover">
	<thead>
		<tr>
			<th>Nội dung</th>
			<th>#</th>
		</tr>
	</thead>
	<tbody>
		<?php foreach ($categories as $categoryTempId =>$categoryTitle){ ?>
			<tr <?php echo (isset($categoryId) && $categoryId==$categoryTempId) ? 'class="table-warning"':''; ?> rel='data'>
				<td><a href="/admin/categories/edit/<?= $categoryTempId ?>"><?php echo $categoryTitle; ?></a></td>
				<td>
					<a href="/admin/categories/delete/<?php echo $categoryTempId; ?>" rel='fa' onclick="return confirm('Bài viết này sẽ bị xóa..?')">
						<span class="fa fa-times"></span>
					</a>
				</td>
			</tr>
		<?php } ?>
	</tbody>
</table>
