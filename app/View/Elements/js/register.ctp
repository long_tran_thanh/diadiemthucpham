<script type="text/javascript" src="/js/parsley.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/js/bootstrap-datepicker.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/css/bootstrap-datepicker3.css"/>

<script type="text/javascript" >
$(document).ready(function(){
    var order = [];
    var inc = 1;

    $('input[name=province_name]').val($('select[ref=province_code] option:selected').text())    
    $('input[name=district_name]').val($('select[ref=district_code] option:selected').text())

    $('#order_form').parsley({
        'excluded':'[disabled]'
    })

    $('input[dp=datepicker]').datepicker({
        format: 'dd-mm-yyyy'        
    })

    $('select[name=sell_food_schedule').change(function(){
        let sfs_value = $(this).val()
        $('div[ref=sell_food_schedule]').hide()
        $('div[ref=sell_food_schedule] input').val('')
        $('div[ref=sell_food_schedule][vd='+sfs_value+']').show();
    })

    $('input[name=method').change(function(){
        let m_value = $(this).val()
        $('div[ref=method]').hide()
        $('div[ref=method] input, textarea, select').prop('disabled', true);
        $('div[ref=method][vd='+m_value+']').show();
        $('div[ref=method][vd='+m_value+'] input, textarea, select').prop('disabled', false);

        if(m_value == 'FREE'){
            $('div[ref=FOOD_TYPE]').hide()
        }else{
            $('div[ref=FOOD_TYPE]').show()
        }
    });

    $('input[name=food_type]').change(function(){
        let f_value =  $(this).val()
        $('div[ref=method]').hide()
        $('div[ref=method] input, textarea, select').prop('disabled', true);
        $('div[ref=method][vd='+f_value+']').show();
        $('div[ref=method][vd='+f_value+'] input, textarea, select').prop('disabled', false);
    })


    $('select[ref=province_code]').change(function(){
        $.get('/cadastrals/?province_code=' + $(this).val(), function(items){
            $('select[ref=district_code] option').remove()

            $(items).each(function(index, item ){
                $('select[ref=district_code]').append('<option value="'+item.Cadastral.district_code+'">'+item.Cadastral.district_name+"</option>")
            })
        },'json')

        $('input[name=province_name]').val($('select[ref=province_code] option:selected').text())
    })


    $('select[ref=district_code]').change(function(){
        $.get('/cadastrals/?district_code=' + $(this).val(), function(items){
            $('select[ref=ward_code] option').remove()
            $(items).each(function(index, item ){

                $('select[ref=ward_code]').append('<option value="'+item.Cadastral.ward_code+'">'+item.Cadastral.ward_name+"</option>")
            })
        },'json')

        $('input[name=district_name]').val($('select[ref=district_code] option:selected').text())
    })

    $('select[ref=ward_code]').change(function(){
        $('input[name=ward_name]').val($('select[ref=ward_code] option:selected').text())
    })



    $('button[ref=add_item_order]').click(function(){
        let elemtnts = $(this).parent().find('[ref=order-items]')
        let product_name = $(this).parent().find('input[name=product_name]').val()
        let amount = $(this).parent().find('input[name=amount]').val()
        if($.trim(product_name) == "" || $.trim(amount) == ""){
            alert("Thiếu thông tin loại thực phẩm hoặc số lượng ")
            return 
        }
        let product = {}
        $(elemtnts).each(function(index, item){
            let product_name = $(item).attr('name')
            let product_val = $(this).val()
            product[product_name] = product_val
        });

        product['method'] = $('input[name=method]:checked').attr('label')
        order.push(product)
        displayOrderList()

        $('input[name=orders]').val(JSON.stringify(order))

        // Clean data
        $(elemtnts).each(function(index, item){
            if($(item).attr('name') !== 'from_date' && 
                $(item).attr('name') !== 'to_date' && 
                $(item).attr('name') !== 'schedule' &&
                $(item).attr('name') !== 'product_type'){
                $(item).val('')
            }
        });
    })

    $('body').on('click', 'a[ref=remove_order_item]',function(){
        let product_id = $(this).attr('pid')
        let t_order = []
        
        $(order).each(function(index,item){
            if (item.id != product_id){
                t_order.push(item)
            }
        })

        order = t_order
        displayOrderList()
    })

    function displayOrderList(){        
        $('#tbody_order_list tr').remove()
        let ord = 1 

        if(order.length == 0){
            $('#tbody_order_list').append(
                $('<tr />').append(
                    $('<td />', {text:'Không có dữ liệu đơn hàng', colspan:"10", class:"text-center"})
                )
            )
        }

        $(order).each(function(index,item){
            let tr = $('<tr >')
                tr.append($('<td />', {text: ord }));
                tr.append($('<td />', {text:item.method }));
                tr.append($('<td />', {text:item.product_type }));
                tr.append($('<td />', {text:item.product_name}));
                tr.append($('<td />', {text:item.amount }));

                let price = ''
                if(item.hasOwnProperty('price')){
                    price = parseFloat(item.price)
                    if(price < 0 || isNaN(price)){
                        price = 0
                    }                    
                    price = price.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,') 
                }            
                tr.append($('<td />', {text: price, class:'text-right'})); // Giá ban

                let order_time = ''
                if(item.hasOwnProperty('from_date') && item.hasOwnProperty('to_date') && item.hasOwnProperty('schedule')){
                    if(item.schedule == ''){
                        order_time = 'Từ ' + item.from_date + ' đến ' + item.to_date
                    }else{
                        order_time = item.schedule
                    }
                }
                tr.append($('<td />', {text:order_time})); // Thời gian 
                tr.append($('<td />', {text:item.desc })); // Ghi chú    
                tr.append($('<td />', {html: '<a href="javascript:void(0)" pid="'+ ord +'" ref="remove_order_item"> [Xóa] </a>'})); // xóa

            order[index].id = ord
            ord++;

            $('#tbody_order_list').append(tr)
        });
    }
});
</script>