<script type="text/javascript" src="/js/parsley.min.js"></script>
<script>
var map, featureList, boroughSearch = [], theaterSearch = [], museumSearch = [];
var mapBoxLayer = L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw', {
	maxZoom: 18,
	attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors, ' +
		'Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
	id: 'mapbox/streets-v11',
	tileSize: 512,
	zoomOffset: -1
})

$(window).resize(function() {
	sizeLayerControl();
});

/* Larger screens get expanded layer control and visible sidebar */
if (document.body.clientWidth <= 767) {
	var isCollapsed = true;
} else {
	var isCollapsed = false;
}

var baseLayers = {
	"Mapbox": mapBoxLayer
};

var theaterLayer = L.geoJson(null);
var museumLayer = L.geoJson(null);
var boroughs = L.geoJson(null);
var subwayLines = L.geoJson(null);

var groupedOverlays = {
	"Cho thực phẩm, đồ ăn": {
		"Theaters": theaterLayer,
		"Museums": museumLayer
	},
	"Cửa hàng mua bán": {
		"Boroughs": boroughs,
		"Subway Lines": subwayLines
	}
};

var map = L.map("map", {
	zoom: 10,
	center: [10.762622, 106.660172],
	layers: [mapBoxLayer],
	zoomControl: true,
	attributionControl: true
});


var info = L.control();
	info.onAdd = function (map) {
		this._div = L.DomUtil.create('div', 'info'); 
		this.update();
		return this._div;
	};

	info.update = function (props) {
		this._div.innerHTML = '<p class="title-legend"> Thông tin địa điểm thực phẩm </p> <hr/>';	
		if (props != undefined){
			this._div.innerHTML = this._div.innerHTML  + props;
		}		
	};

	info.addTo(map);
 
 
//$posts

//cá nhân + cho -> nguoi tu tien.jpg
var givingPerson = L.icon({
    iconUrl: "/images/nguoi-tu-tien.png", 
    shadowUrl: "/images/shadow-point.png}",
	iconSize: [32, 32]
});

//tổ chức + cho -> cua hang tu thien.jpg
var charityShop = L.icon({
    iconUrl: "/images/cua-hang-tu-thien.jpg", 
    shadowUrl: "/images/shadow-point.png",
	iconSize: [32, 32]
});

//hộ gia đình + cho -> gia dinh tu thien.jpg
var businessPerson = L.icon({
    iconUrl: "/images/gia-dinh-tu-thien.jpg", 
    shadowUrl: "/images/shadow-point.png",
	iconSize: [32, 32]
});

//xe lưu động + cho -> xe luu dong tu thien.jpg
var givingCar = L.icon({
    iconUrl: "/images/xe-luu-dong-tu thien.jpg", 
    shadowUrl: "/images/shadow-point.png",
	iconSize: [32, 32]
});

// #### Bán ##########

//cá nhân + bán -> nguoi ban.jpg
var salePerson = L.icon({
    iconUrl: "/images/nguoi-ban.jpg", 
    shadowUrl: "/images/shadow-point.png",
	iconSize: [32, 32]
});

//tổ chức + bán -> cua hang ban.jpg
var saleShop = L.icon({
    iconUrl: "/images/cua-hang-ban.jpg", 
    shadowUrl: "/images/shadow-point.png",
	iconSize: [32, 32]
});

//hộ gia đình + bán -> ho gia dinh ban.jpg
var saleBusinessShop = L.icon({
    iconUrl: "/images/ho-gia-dinh-ban.jpg", 
    shadowUrl: "/images/shadow-point.png",
	iconSize: [32, 32]
});


//xe lưu động + bán -> xe luu dong ban.jpg
var saleCar = L.icon({
    iconUrl: "/images/xe-luu-dong-ban.jpg", 
    shadowUrl: "/images/shadow-point.png",
	iconSize: [32, 32]
});

var points = {
	"type": "FeatureCollection",
	"features": [
        <?php $totalPoint = count($points); ?>
		<?php foreach($points as $id => $point){?>
		{
			"geometry": {
				"type": "Point",
				"coordinates": [<?php echo $point['Point']['long'];?>, <?php echo $point['Point']['lat'];?>]
			},
			"type": "Feature",
			"properties": {
				"popupContent": "<?php echo $point['Point']['name'];?>"
			},
			"is_free": "<?php echo $point['Point']['is_free'];?>",
			"is_sale": "<?php echo $point['Point']['is_sale'];?>",
			"org_type": "<?php echo $point['Point']['org_type'];?>",
			"id": <?php echo $point['Point']['id'];?>
		}
        <?php $totalPoint = $totalPoint - 1;?>
        <?php if($totalPoint > 0){?>,<?php } ?>
        <?php } ?>
	]
};

var geoJson = L.geoJSON([points], {	
	onEachFeature: onEachFeature,
	pointToLayer: function (feature, latlng) {
		return L.marker(latlng, {'icon': setIconPointer(feature)});
	},

}).addTo(map);

map.fitBounds (geoJson.getBounds())


//var stores = L.layerGroup([points]);

function onEachFeature(feature, layer) {
	layer.on('click', function (e) {
	  	$.get(
			'/information/?order_id=' + feature.id,
			function(resp){
				info.update(resp)
				
			}
		)		
    });
}





function sizeLayerControl() {
	$(".leaflet-control-layers").css("max-height", $("#map").height() - 50);
}

function setIconPointer(feature){
	if (feature.is_free == "True"){
		if(feature.org_type == "Cá nhân"){
			return givingPerson
		}else if(feature.org_type == "Tổ chức"){
			return charityShop
		}else if(feature.org_type == "Hộ gia đình"){
			return businessPerson
		}else if(feature.org_type == "Xe lưu động"){
			return givingCar
		}
	}else{
		if(feature.org_type == "Cá nhân"){
			return salePerson
		}else if(feature.org_type == "Tổ chức"){
			return saleShop
		}else if(feature.org_type == "Hộ gia đình"){
			return saleBusinessShop
		}else if(feature.org_type == "Xe lưu động"){
			return saleCar
		}
	}
}

$('td[ref=infor]').click(function(){
	let order_id = $(this).attr('order-id')
	
	$.each(geoJson._layers, function(object_id, item ){
		if(item.feature.id == order_id){
			map.setView(new L.LatLng(item._latlng.lat, item._latlng.lng), 15);
			$.get(
				'/information/?order_id=' + order_id,
				function(resp){
					info.update(resp)
					
				}
			)	
		}
	})
});

$('body').on('click', 'button[ref=enroll]', function(){
	let order_id = $(this).attr('order-id')

	$('#enroll-modal').find('input[name=order_id]').val(order_id)
	$('#enroll-modal').modal();

})

$('#btn-pick-order').click(function(){

	if($('#enroll-form').parsley().validate()){
		$.post('/', $('#enroll-form').serialize(), function(resp){
			alert('Đăng ký thành công! ');
			$('#enroll-modal').modal('toggle')
			
		});
	}	
})


</script>