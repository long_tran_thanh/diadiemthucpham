<br/>


<div class="row">
	<div class="col-md-4">
		<h4>Upload hình ảnh cho hệ thống!</h4>
		<form method="POST" class="form" action="/admin/images/upload" enctype="multipart/form-data" accept-charset="utf-8">
			<div class="form-group">
				<input id="upload_file" type="file" name="data[ImageFile][]" value="" multiple />
			</div>
			<div class="form-group">
				<label for="exampleInputName2"> Thư mục upload : &nbsp;</label>
				<select name="data[folder]" id="folder" class="form-control">
					<option value="uploads">Thư viện ảnh  </option>
					<option value="slider"> Hình ảnh banner </option>
				</select>
			</div>
			<div class="form-group preview" >
				<label> Hình ảnh sẽ upload  </label>
				<br/>
				<img id="image_preview" src="#" />
			</div>
			<button class="btn btn-info" type="submit" name="btnUpload"> Upload hình </button>
		</form>
	</div>
</div>

<div class="clearfix"></div>
<br/>
<ul class="nav nav-tabs">
    <li class="nav-item">
		<a href="#library"  class="nav-link active" data-toggle="tab">Hình ảnh đã upload</a>
	</li>
    <li class="nav-item">
		<a href="#slider"  class="nav-link" data-toggle="tab">Hình ảnh cho banner dọc </a>
	</li>
</ul>


  <!-- Tab panes -->
<div class="tab-content">

    <div  class="tab-pane container-fluid active" id="library">
		<br/>
		<table class="table table-bordered">
			<?php if(!empty( $imageUploaded)){
				$totalCols = count($imageUploaded);
				$col = 0;
				foreach ($imageUploaded as $file => $image) {
					if($col == 0){
						echo '<tr>';
					}
					echo '<td>'. $image .' <br/>
						<a href="/admin/images/delete/?file='.$file.'"  onclick="return confirm(\'Hình ảnh này sẽ bị xóa bỏ..?\')"> Xóa </a> |
						<a href="javascript:void(0)" ref="copy"> Copy </a>

					</td>';
					if($col==5){
						$col = 0;
						echo '</tr>';
					}else{
						$col++;
					}
				}
			} ?>
		</table>
    </div>

  	<div  class="tab-pane container-fluid fade" id="slider">
    	<br/>

		<table class="table table-bordered">
		<?php $totalCols = count($imageSlider); ?>
		<?php $col = 0;?>
		<?php foreach ($imageSlider as $file => $image) { ?>
			<tr>
				<td width="18%">
					<img style="max-width:250px" src="/images/slider/<?php echo  $image['Slider']['image'];?>" class="img-thumbnail"/>
					<p>
						<a href="/admin/images/bannerdelete/?id=<?php echo $image['Slider']['id'];?>"  onclick="return confirm(\'Hình ảnh này sẽ bị xóa bỏ..?\')"> Xóa </a>
					</p>
				</td>
				<td>
				<label>Link liên kết </label>
				<input type="text" placeholder="Link khi click" class="form-control" name="slider_<?php echo $image['Slider']['id'];?>" data-type="text1" data-id="<?php echo $image['Slider']['id'];?>"
						rel="slider_text" value="<?php echo $image['Slider']['text1'];?>">

				<label>Thứ tự  </label>
				<input type="number" placeholder="Link khi click" class="form-control" name="slider_<?php echo $image['Slider']['id'];?>" data-type="position" data-id="<?php echo $image['Slider']['id'];?>"
						rel="slider_text" value="<?php echo $image['Slider']['position'];?>">
				</td>
			</tr>
		<?php } ?>
		</table>
    </div>

</div>


<script type="text/javascript">

	$(document).ready(function(){
		$('.preview').hide();
		$('input[rel=slider_text]').change(function(){
			var id=$(this).attr('data-id');
			var field = $(this).attr('data-type');
			var value = $(this).val();
			value = value.toString();
			if(id!= null && field!= null){
				$.get( '/admin/images/text?id='+id+'&field='+field+'&val='+ value,function(){
					toastr.success("Đã cập nhật thông tin!");
				});
			}
		});

		$('select[rel=slider-position]').change(function(){
			var id=$(this).attr('data-id');
			var field = $(this).attr('data-type');
			var value = $(this).val();

			if(id!= null && field!= null){
				$.get('/admin/images/text?id='+id+'&field='+field+'&val='+value);

			}
		});


        $("#upload_file").change(function() {
        	var e = $(this).prop("files")[0],
	            t = new FileReader;
	        return e.type.match("image.*") ? (t.onload = function() {
	        	$('.preview').show();
	            $("#image_preview").attr("src", t.result).width(150);

	        	}, t.readAsDataURL(e), void 0) : (alert("Không thể load được hình ảnh"), void 0)
	   		})
		});

		$('input[rel=customer]').focusout(function(){
			var id= $(this).attr('data-id');
			var field = $(this).attr('field');
			var value = $(this).val();

			if(id!= null && field!= null){
				$.get('/admin/images/customer?id='+id+'&field='+field+'&val='+value);

			}
		});

		$('a[ref=copy]').click(function(){
			var img_src = '<?php echo SERVER;?>' + $(this).parent().find('img').attr('src')

			var $temp = $("<input>")
			$("body").append($temp);

			$temp.val(img_src).select();
			document.execCommand("copy");
			$temp.remove();

			toastr.success("Đã ghi nhớ đường dẫn ")
		})


</script>