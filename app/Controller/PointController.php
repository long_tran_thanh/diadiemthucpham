<?php


class PointController extends AppController{
    public $name = 'Point';
    public $layout = 'admin';
    public $uses = array('Point','PointItem', 'Customer');
    public $paginate = array(
        'limit'=> 100,
        'order'=> 'created_at DESC'

    );
    public $components = array('Paginator','Session','Auth','Upload');

    /**
    * [beforeFillter description]
    * @return [type] [description]
    */
    function beforeFilter(){
        parent::beforeFilter();
        $this->Auth->flashElement = null;
        $this->Auth->allow('view');
        $this->Auth->loginError = "Sai tên đăng nhập hoặc mật khẩu";
        $this->Auth->authError  = "Phiên đăng nhập đã kết thúc, phải đăng nhập lại";
        $this->Auth->userModel = 'User';
        $this->Auth->fields = array('username' => 'email', 'password' => 'password');
        $this->Auth->loginAction = array('controller' => 'users', 'action' => 'login');
        $this->Auth->loginRedirect = array('controller' => 'users', 'action' => 'admin_index');
        $this->Auth->allow('introduction','recruitment','support','shedule','travel','travel_detail');

    }

    /**
    * [index description]
    * @return [type] [description]
    */
    public function admin_index(){
        $this->layout = 'admin';
        $conditions = array();

        $this->Paginator->settings = $this->paginate;
        $pointData = $this->Paginator->paginate('Point',$conditions);
        // Create beardcrumb
        $breadCurmb = array(
            'title'=>array('title'=>'Quản lý các địa điểm thực phẩm'),
            'path'=>array(
                array('link'=>SERVER,'title'=>'Trang chủ'),
                array('link'=>SERVER.'admin/page/index','title'=>'Quản lý các địa điểm thực phẩm'),
                array('link'=>'','title'=>'','active'=>1)
            )
        );

        $this->set('breadCurmb',$breadCurmb);
        $this->set('pointData',$pointData);
    }


    public function admin_detail(){        
        $this->layout = 'ajax';
        $conditions = array(
            'conditions'=>array(
                'order_id'=> $this->request->query['order_id']
            )
        );
        
        $pointItems = $this->PointItem->find('all', $conditions);
        $this->set('pointItems', $pointItems);        
    }


    public function admin_toogle() {
        $this->autoRender=false;
		
        $point = $this->Point->findById($this->request->query['order_id']);
        if(empty($point)){
            $this->Session->setFlash('Không có dữ liệu để cập nhật!.','flash/error');
            $this->redirect(array('action'=>'admin_index'));
        }

        $point['Point']['actived'] = ($point['Point']['actived'] == true ? false : true);

        $this->Point->set($point);
        $this->Point->save();
        
        $this->Session->setFlash('Dữ liệu đã được cập nhật!.','flash/success');
        $this->redirect(array('action'=>'admin_index'));
	}

    public function admin_enroll() {
        $breadCurmb = array(
            'title'=>array('title'=>'Danh sách người nhận/mua '),
            'path'=>array(
                array('link'=>SERVER,'title'=>'Trang chủ'),
                array('link'=>SERVER.'admin/point/index','title'=>'Danh sách người nhận/mua'),
                array('link'=>'','title'=>'','active'=>1)
            )
        );

        $condition = array(
            'conditions'=>array(
                'order_id' => $this->request->query['order_id']
            ),
            'order'=>'id ASC'
        );

        $customers = $this->Customer->find('all', $condition);
        $this->set('customers', $customers);
        $this->set('breadCurmb',$breadCurmb);
	}



}
