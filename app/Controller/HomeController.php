<?php

class HomeController extends AppController{
	public $name ='Home';
	public $layout = 'default';
	public $uses = array('Category', 'Point','PointItem','Cadastral','Customer');

	public function beforeFilter(){
		parent::beforeFilter();
	}

	public function index(){

		// Đăng ký nhận (mua ) hàng
		if(!empty($this->data)){
			$this->enrollOrder($this->data);
			die();
		}

		# Lấy các địa điểm
		$condition = array(
			'conditions'=> array(
				'actived'=> True 
			),
			'order' => 'id'
		);

		$points = $this->Point->find('all', $condition);
		$this->set('points', $points);
		$this->set('useMapControl', true);
	}

	public function enrollOrder($data){
		$customer = array();
		$customer['Customer']['name'] 			= $data['fullname'];
		$customer['Customer']['phone'] 			= $data['phone'];
		$customer['Customer']['created_at'] 	= date('Y-m-d H:i:s');
		$customer['Customer']['order_id'] 		= $data['order_id'];

		$this->Customer->set($customer);
		$this->Customer->save();

		$point = $this->Point->findById($data['order_id']);
		if(!empty($point)){
			$point['Point']['total_enroll'] =  $point['Point']['total_enroll']  + 1;

			$this->Point->set($point);
			$this->Point->save();
		}
		
		return True;
	}

	public function detail(){
		$this->layout = 'ajax';		
		if (!isset($this->request->query['order_id'])){
			die('Không tim thấy dữ liệu về địa điểm này ');
		}

		$orderId = $this->request->query['order_id'];

		$point = $this->Point->findById($orderId);		
		$condition = array(
			'conditions'=> array(
				'order_id'=> $orderId
			)
		);

		$pointItems = $this->PointItem->find('all', $condition);
		if (empty($pointItems)){
			die('Không tim thấy dữ liệu về địa điểm này!... ');
		}
		
		$this->set('pointItems', $pointItems);
		$this->set('point', $point);
	}


	public function register(){
		$this->set('useRegister', True);

		if(!empty($this->data)){
			$this->updatePointInformation($this->data);
			$this->redirect('/dang-ky-thanh-cong');
		}

		$condition = array(			
			'fields'=>array('DISTINCT province_code', 'province_name'),
			'order'=>'province_code ASC'
		);

 		$provinces = $this->Cadastral->find('all', $condition);

		$condition = array(			
			'fields'=>array('DISTINCT district_code', 'district_name'),
			'conditions'=> array(
				'province_code'=> 79
			),
			'order'=>'district_code ASC'
		);

 		$districts = $this->Cadastral->find('all', $condition);
		$this->set('districts',$districts);
		$this->set('provinces',$provinces);
	}

	public function report(){
		$ticketCode = $this->Session->read(SESSION_TICKET_CODE);
		if(empty($ticketCode)){
			$this->redirect('/');
		}
		// Remove session 
		$this->Session->delete(SESSION_TICKET_CODE);
	}


	public function cadastral(){
		$this->autoRender=false;
		$condition = array();
		
		if(isset($this->request->query['province_code'])) {
			$condition = array(			
				'fields'=>array('DISTINCT district_code', 'district_name'),
				'conditions'=> array(
					'province_code'=> $this->request->query['province_code']
				),
				'order'=>'district_code ASC'
			);
		}

		if(isset($this->request->query['district_code'])){
			$condition = array(			
				'fields'=>array('DISTINCT ward_code', 'ward_name'),
				'conditions'=> array(
					'district_code'=> $this->request->query['district_code']
				),
				'order'=>'district_code ASC'
			);
		}

		if(empty($condition)){
			die(json_encode(array('message'=> 'Không tìm thấy dữ liệu')));
		}

		$data = $this->Cadastral->find('all', $condition);
		die(json_encode($data));
	}

	public function updatePointInformation($data){


		$point = array();
		$point['Point']['org_type'] = $data['org_type'];
		$point['Point']['name'] = $data['org_name'];
		$point['Point']['phone'] = $data['org_phone'];
		$point['Point']['address'] = $data['org_address'];

		$point['Point']['province_name'] = trim($data['province_name']);
		$point['Point']['province_id'] = $data['org_province'];
		$point['Point']['district_name'] = trim($data['district_name']);
		$point['Point']['district_id'] = $data['org_district'];
		$point['Point']['ward_name'] = trim($data['ward_name']);
		$point['Point']['ward_id'] = @$data['org_ward'];
		$point['Point']['created_at'] = date('Y-m-d H:i:s');
		$point['Point']['total_enroll'] =0 ;

		
		// Call google API 
		$formattedAddress =  trim($data['org_address']);
		if(trim($data['ward_name'])!=""){
			$formattedAddress = $formattedAddress . " " . trim($data['ward_name']);
		}

		if(trim($data['district_name'])!=""){
			$formattedAddress = $formattedAddress . " " . trim($data['district_name']);
		}

		if(trim($data['province_name'])!=""){
			$formattedAddress = $formattedAddress . " " . trim($data['province_name']);
		}

		$params = array(
			'key' => 'AIzaSyDue9aZJqIzFrKZL_Flt4XP03BmC_jWuKM',
			'address' => $formattedAddress
		);
		$query = http_build_query($params);


		$googleResp = file_get_contents('https://maps.googleapis.com/maps/api/geocode/json?'. $query);
		$googleRespJson = json_decode($googleResp, true);

		if(!empty($googleRespJson['results'])){
			$point['Point']['lat'] = @$googleRespJson['results'][0]['geometry']['location']['lat'];
        	$point['Point']['long'] = @$googleRespJson['results'][0]['geometry']['location']['lng'];			
        	$point['Point']['formatted_address'] = @$googleRespJson['results'][0]['formatted_address'];
		}

		// Save 
		$this->Point->set($point);
		$this->Point->save();

		$orderItems = json_decode($data['orders'], true);
		
		$isSale = false;
		$isFree = false;
		$totalProduct = count($orderItems);

		$pointItems = array();
		foreach($orderItems as $orderItem){
			$item = array();
			$item['PointItem']['order_id'] = $this->Point->id;
			$item['PointItem']['method'] = $orderItem['method'];
			$item['PointItem']['product_name'] = $orderItem['product_name'];
			$item['PointItem']['amount'] = $orderItem['amount'];
			
			if(!empty($orderItem['product_type'])){
				$item['PointItem']['product_type'] =  $orderItem['product_type'];
			}

			if(!empty($orderItem['price'])){
				$item['PointItem']['price'] =  $orderItem['price'];
			}

			$item['PointItem']['schedule'] = $orderItem['schedule'];
			$item['PointItem']['desc'] = $orderItem['desc'];
			$item['PointItem']['created_at'] = date('Y-m-d H:i:s');

			if($orderItem['from_date']!=""){
				$item['PointItem']['start_date'] = date('Y-m-d', strtotime($orderItem['from_date']));
			}

			if($orderItem['to_date']!=""){
				$item['PointItem']['end_date'] = date('Y-m-d', strtotime($orderItem['to_date']));
			}
			
			$pointItems[] = $item;
			
			if($orderItem['method'] === "Cho" && $isFree == false){
				$isFree = True;
			}

			if($orderItem['method'] === "Bán" && $isSale == false){
				$isSale = True;
			}
		}

		if(!empty($pointItems)){
			$this->PointItem->saveMany($pointItems);
		}

		// Update Point again
		$point['Point']['id'] =  $this->Point->id;

		$point['Point']['is_free'] =  $isFree;
		$point['Point']['is_sale'] =  $isSale;
		$point['Point']['total_product'] =  $totalProduct;

		$this->Point->set($point);
		$this->Point->save();

		// Save session 
		$this->Session->write(SESSION_TICKET_CODE, md5(uniqid(rand(), true)));
		return True;
	}
}

?>


