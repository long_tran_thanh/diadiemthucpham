<?php
/**
 * Application level Controller
 *
 * This file is application-wide controller file. You can put all
 * application-wide controller-related methods here.
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */


App::uses('Controller', 'Controller');

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @package		app.Controller
 * @link		http://book.cakephp.org/2.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller {
	public $components = array('Cookie','Session','Captcha');
	public $uses = array('Category','Page');
	public $is_mobile = false;


	/**
	 * [beforeFillter description]
	 * @return [type] [description]
	 */
	public function beforeFilter()	{
		parent::beforeFilter();
		// Set title
		$websiteInfo = $this->getJsonDataFile();
		$menuHeader = $this->Category->getCategoryList(MENU_WEBSITE_ID,"all", "ord ASC ");
		$this->set('menuHeader', $menuHeader);

		$this->set('websiteInfo',$websiteInfo);
		$this->set('token_key', md5(uniqid(rand(), true)));
	}


    

	/**
	 * [countFileUpload description]
	 * @return [type] [description]
	 */
	public function countFileUpload($insertFile = array()){
		if(empty($insertFile) || (isset($insertFile[0]['size'])  && $insertFile[0]['size']==0 )){
			return false;
		}

		$totalFileUpload = 0;
		foreach ($insertFile as $image) {
			if($image['size'] > 0){
				$totalFileUpload++;
			}
		}

		return $totalFileUpload;
	}



	/**
	 * [getFooter description]
	 * @return [type] [description]
	 */
	public function getFooter(){
		$this->loadModel('Page');
		$footer = $this->Page->getByKey('address-footer');
		$this->set('footer', $footer);
	}

	/**
	 * [layoutForFrontEnd description]
	 * @return [type] [description]
	 */
	public function layoutForFrontEnd(){
		$menuHeader = $this->Category->getCategoryData(WEBSITE_CATEGORY_ID);
		// Get slider
		$slider = $this->getSlider();
		//Get footer
		$this->getFooter();

		$this->set('menuHeader', $menuHeader);
		$this->set('title_for_content',SITE_NAME);
		$this->set('slider',$slider);

	}


	/**
	 * [getJsonDataFile description]
	 * @param  [type] $fileExport [description]
	 * @return [type]             [description]
	 */
	public function getJsonDataFile($fileExport = CONFIGURATION_FILE){
		if (!file_exists($fileExport)) {
				return null;
		}

		$jsonExrate = file_get_contents($fileExport);
		return json_decode($jsonExrate, true);
	}

	/**
	 * [setJsonDataFile description]
	 * @param [type] $jsonData [description]
	 */
	public function setJsonDataFile($jsonData = null, $fileExport = CONFIGURATION_FILE){
		if ($jsonData == null || $fileExport == null) {
			return null;
		}

		// Write file
		$handle = fopen($fileExport, 'w+');
		fwrite($handle, $jsonData);
		fclose($handle);

		return true;
	}

	/**
	 * [getImageLibrary description]
	 * @return [type] [description]
	 */
	public function getImageLibrary(){
		$handle = opendir(LIBRARY_DIR.DS.'library' );
		$imageLibrary = array();
        while($file = readdir($handle)){
            if($file !== '.' && $file !== '..' ){
                $imageLibrary[] = $file;
            }
        }
        return $imageLibrary;
	}

}
